#ifndef CARDFACTORY_H
#define CARDFACTORY_H

class Card;

/**
 * @brief Fabryka kart.
 *
 */
class CardFactory {
private:
    CardFactory() {}
public:

    /**
     * @brief Tworzy kartę o danym numerze ze zbioru.
     * @param setId
     * @param id
     * @return
     */
    static Card* createCardBySetId(int setId, int id);

    /**
     * @brief Zwraca ilość kart w zbiorze.
     * @return
     */
    static int getCardNumber();
};

#endif // CARDFACTORY_H
