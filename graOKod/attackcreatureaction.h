#ifndef ATTACKCREATUREACTION_H
#define ATTACKCREATUREACTION_H
#include "action.h"

class Card;

/**
 * @brief Klasa przechowuje informacje o akcji 'atak na kreaturę'.
 *
 */
class AttackCreatureAction : public Action {
private:
    Card* attacker;
    Card* defender;

public:
    /**
     * @brief Konstruktor AttackCreatureAction
     * @param attacker_
     * @param defender_
     */
    AttackCreatureAction(Card* attacker_, Card* defender_);
    ~AttackCreatureAction() {}

    Card* getAttacker();
    Card* getDefender();
};

#endif // ATTACKCREATUREACTION_H
