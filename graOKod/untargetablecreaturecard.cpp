#include "untargetablecreaturecard.h"

bool UntargetableCreatureCard::isUntargetable() {
    return true;
}

UntargetableCreatureCard14::UntargetableCreatureCard14(int id_) {
    id = id_;
    setId = 14;
    name = "Assembler";
    attack = 2;
    health = 2;
    cost = 2;
    exhausted = false;
    desctription = "NIENAMIERZALNY";
}
