#include "card.h"


Card::~Card() {}

int Card::getId() {
    return id;
}

int Card::getCost() {
    return cost;
}

bool Card::isExhausted() {
    return exhausted;
}

std::string Card::getName() {
    return name;
}

int Card::getSetId() {
    return setId;
}

int Card::getAttack() {
    return attack;
}

int Card::getHealth() {
    return health;
}

void Card::setExhausted(bool exhausted_) {
    exhausted = exhausted_;
}

void Card::increaseHealth(int val) {
    health+=val;
}

void Card::decreaseHealth(int val) {
    health-=val;
}

void Card::increaseAttack(int val) {
    attack+=val;
}

void Card::decreaseAttack(int val) {
    attack-=val;
    attack = (attack < 0)? 0 : attack;
}

bool Card::isProtector() {
    return false;
}

bool Card::isElusive() {
    return false;
}

bool Card::isStealth() {
    return false;
}

bool Card::isUntargetable() {
    return false;
}

bool Card::isFerocity() {
    return false;
}

std::string Card::getDescription() {
    return desctription;
}

