#include "addmanaaction.h"

AddManaAction::AddManaAction(Player *player_, int value_) {
    player = player_;
    value = value_;
}

Player *AddManaAction::getPlayer() {
    return player;
}

int AddManaAction::getValue() {
    return value;
}

