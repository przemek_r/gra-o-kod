#ifndef NORMALCREATURECARD_H
#define NORMALCREATURECARD_H
#include "creaturecard.h"

class NormalCreatureCard : public CreatureCard {
private:

public:
    NormalCreatureCard();
    ~NormalCreatureCard();

};

class NormalCreatureCard5 : public NormalCreatureCard {
public:
    /**
     * @brief Konstruktor NormalCreatureCard5
     * @param id_
     */
    NormalCreatureCard5(int id_);
    ~NormalCreatureCard5() {}
};

class NormalCreatureCard8 : public NormalCreatureCard {
public:
    /**
     * @brief Konstruktor NormalCreatureCard8
     * @param id_
     */
    NormalCreatureCard8(int id_);
    ~NormalCreatureCard8() {}
};

class NormalCreatureCard9 : public NormalCreatureCard {
public:
    /**
     * @brief Konstruktor NormalCreatureCard9
     * @param id_
     */
    NormalCreatureCard9(int id_);
    ~NormalCreatureCard9() {}

    void putAction(ActionManager* actionManager, Card* target);
};

class NormalCreatureCard11 : public NormalCreatureCard {
public:
    /**
     * @brief Konstruktor NormalCreatureCard11
     * @param id_
     */
    NormalCreatureCard11(int id_);
    ~NormalCreatureCard11() {}
};

class NormalCreatureCard12 : public NormalCreatureCard {
public:
    /**
     * @brief Konstruktor NormalCreatureCard12
     * @param id_
     */
    NormalCreatureCard12(int id_);
    ~NormalCreatureCard12() {}
};
#endif // NORMALCREATURECARD_H
