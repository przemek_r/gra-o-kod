#include "cardmanager.h"
#include <algorithm>
#include <functional>
#include "card.h"
#include "deck.h"
#include "game.h"
#include "player.h"

void cardManager::moveCard(Card* card_, Deck* to_) {
    to_->addCard(card_);
}

void cardManager::moveCard(Card *card_, Deck* from_, Deck* to_) {
    using namespace std::placeholders;

    // usuwamy kartę z jednej talii
    auto end = std::remove_if(
        from_->getCards().begin(),
        from_->getCards().end(),
        std::bind(std::equal_to<Card*>(), _1, card_)
    );
    from_->getCards().erase(end, from_->getCards().end());
    to_->addCard(card_); // dodajemy kartę do drugiej talii
}

Player *cardManager::getCardOwner(Card *card_, Game *game_) {
    using namespace std::placeholders;
    Player* playerOne = game_->getPlayerOne();
    Player* playerTwo = game_->getPlayerTwo();

    // przeszukujemy wszystkie talie w poszukiwaniu danej karty.
    if(playerOne->getStackDeck()->getCards().end() != std::find_if(playerOne->getStackDeck()->getCards().begin(),
                                                                   playerOne->getStackDeck()->getCards().end(),
                                                                   std::bind(std::equal_to<Card*>(), _1, card_))) {
        return playerOne;
    }
    if(playerTwo->getStackDeck()->getCards().end() != std::find_if(playerTwo->getStackDeck()->getCards().begin(),
                                                                   playerTwo->getStackDeck()->getCards().end(),
                                                                   std::bind(std::equal_to<Card*>(), _1, card_))) {
        return playerTwo;
    }
    if(playerOne->getHandDeck()->getCards().end() != std::find_if(playerOne->getHandDeck()->getCards().begin(),
                                                                  playerOne->getHandDeck()->getCards().end(),
                                                                  std::bind(std::equal_to<Card*>(), _1, card_))) {
        return playerOne;
    }
    if(playerTwo->getHandDeck()->getCards().end() != std::find_if(playerTwo->getHandDeck()->getCards().begin(),
                                                                  playerTwo->getHandDeck()->getCards().end(),
                                                                  std::bind(std::equal_to<Card*>(), _1, card_))) {
        return playerTwo;
    }
    if(playerOne->getTableDeck()->getCards().end() != std::find_if(playerOne->getTableDeck()->getCards().begin(),
                                                                   playerOne->getTableDeck()->getCards().end(),
                                                                   std::bind(std::equal_to<Card*>(), _1, card_))) {
        return playerOne;
    }
    if(playerTwo->getTableDeck()->getCards().end() != std::find_if(playerTwo->getTableDeck()->getCards().begin(),
                                                                   playerTwo->getTableDeck()->getCards().end(),
                                                                   std::bind(std::equal_to<Card*>(), _1, card_))) {
        return playerTwo;
    }
    if(playerOne->getRemovedDeck()->getCards().end() != std::find_if(playerOne->getRemovedDeck()->getCards().begin(),
                                                                     playerOne->getRemovedDeck()->getCards().end(),
                                                                     std::bind(std::equal_to<Card*>(), _1, card_))) {
        return playerOne;
    }
    if(playerTwo->getRemovedDeck()->getCards().end() != std::find_if(playerTwo->getRemovedDeck()->getCards().begin(),
                                                                     playerTwo->getRemovedDeck()->getCards().end(),
                                                                     std::bind(std::equal_to<Card*>(), _1, card_))) {
        return playerTwo;
    }
}

Card *cardManager::getCardById(int id_, Game *game_) {
    using namespace std::placeholders;
    Player* playerOne = game_->getPlayerOne();
    Player* playerTwo = game_->getPlayerTwo();

    // przeszukujemy wszystkie talie w poszukiwaniu danej karty.
    auto card = std::find_if(playerOne->getStackDeck()->getCards().begin(),
                            playerOne->getStackDeck()->getCards().end(),
                             [id_](Card* card)->bool { return card->getId() == id_;});
    if ( card != playerOne->getStackDeck()->getCards().end()){
        return *card;
    }

    card = std::find_if(playerTwo->getStackDeck()->getCards().begin(),
                            playerTwo->getStackDeck()->getCards().end(),
                             [id_](Card* card)->bool { return card->getId() == id_;});
    if ( card != playerTwo->getStackDeck()->getCards().end()){
        return *card;
    }

    card = std::find_if(playerOne->getHandDeck()->getCards().begin(),
                            playerOne->getHandDeck()->getCards().end(),
                             [id_](Card* card)->bool { return card->getId() == id_;});
    if ( card != playerOne->getHandDeck()->getCards().end()){
        return *card;
    }

    card = std::find_if(playerTwo->getHandDeck()->getCards().begin(),
                            playerTwo->getHandDeck()->getCards().end(),
                             [id_](Card* card)->bool { return card->getId() == id_;});
    if ( card != playerTwo->getHandDeck()->getCards().end()){
        return *card;
    }

    card = std::find_if(playerOne->getTableDeck()->getCards().begin(),
                            playerOne->getTableDeck()->getCards().end(),
                             [id_](Card* card)->bool { return card->getId() == id_;});
    if ( card != playerOne->getTableDeck()->getCards().end()){
        return *card;
    }

    card = std::find_if(playerTwo->getTableDeck()->getCards().begin(),
                            playerTwo->getTableDeck()->getCards().end(),
                             [id_](Card* card)->bool { return card->getId() == id_;});
    if ( card != playerTwo->getTableDeck()->getCards().end()){
        return *card;
    }

    card = std::find_if(playerOne->getRemovedDeck()->getCards().begin(),
                            playerOne->getRemovedDeck()->getCards().end(),
                             [id_](Card* card)->bool { return card->getId() == id_;});
    if ( card != playerOne->getStackDeck()->getCards().end()){
        return *card;
    }

    card = std::find_if(playerTwo->getRemovedDeck()->getCards().begin(),
                            playerTwo->getRemovedDeck()->getCards().end(),
                             [id_](Card* card)->bool { return card->getId() == id_;});
    if ( card != playerTwo->getRemovedDeck()->getCards().end()){
        return *card;
    }
}

