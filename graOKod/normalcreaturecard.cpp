#include "normalcreaturecard.h"
#include "dealdamagetocreatureaction.h"
#include "noselectedcardexception.h"
#include "actionmanager.h"

NormalCreatureCard::NormalCreatureCard() {

}

NormalCreatureCard::~NormalCreatureCard() {

}

NormalCreatureCard5::NormalCreatureCard5(int id_) {
    id = id_;
    setId = 5;
    name = "Prototyp";
    attack = 2;
    health = 1;
    cost = 1;
    exhausted = false;
    desctription = "";
}

NormalCreatureCard8::NormalCreatureCard8(int id_) {
    id = id_;
    setId = 8;
    name = "Jądro";
    attack = 5;
    health = 5;
    cost = 5;
    exhausted = false;
    desctription = "";
}

NormalCreatureCard9::NormalCreatureCard9(int id_) {
    id = id_;
    setId = 9;
    name = "Garbage Collector";
    attack = 2;
    health = 2;
    cost = 4;
    exhausted = false;
    desctription = "Przy zagraniu zadaje 3 obrażenia wybranej kreaturze ";
}

void NormalCreatureCard9::putAction(ActionManager *actionManager, Card *target) {
    if(target == nullptr) {
        throw NoSelectedCardException();
    }

    actionManager->addAction(new DealDamageToCreatureAction(3, target));
}


NormalCreatureCard11::NormalCreatureCard11(int id_) {
    id = id_;
    setId = 11;
    name = "Stos";
    attack = 3;
    health = 2;
    cost = 2;
    exhausted = false;
    desctription = "";
}

NormalCreatureCard12::NormalCreatureCard12(int id_) {
    id = id_;
    setId = 12;
    name = "Sterta";
    attack = 4;
    health = 3;
    cost = 3;
    exhausted = false;
    desctription = "";
}


