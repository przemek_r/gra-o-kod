#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H
#include <string>

class Game;
class ActionManager;
class Card;
class Deck;

enum GameMode{
    HotSeatsMode = 0,
    MultiplayerMode = 1
};

enum GameCreateMode{
    HotSeats = 0,
    CreateMultiplayer = 1,
    JoinMultiplayer = 2
};

/**
 * @brief  Klasa do obsługi rozgrywki.
 *
 */
class GameManager {
private:
    Game* game;
    ActionManager* actionManager;

    /**
     * @brief Akcja pociągnięcia karty.
     */
    void activePlayerDrawCard();

public:
    /**
     * @brief Konstruktor GameManager
     */
    GameManager();
    ~GameManager();

    /**
     * @brief Stworzenie gry.
     */
    void createGame(GameCreateMode gameCreateMode);

    /**
     * @brief Dołączenie gry.
     * @param gameId
     * @param playerId
     * @param opponentId
     * @param opponentName
     * @param opponentDeck
     */
    void joinGame(GameCreateMode gameCreateMode,
                  int opponentId,
                  std::string opponentName,
                  Deck* opponentDeck,
                  int gameId = 0,
                  int playerId = 0);

    /**
     * @brief Rozpoczęcie gry.
     */
    void startGame();


    /**
     * @brief Rozpoczęcie tury.
     */
    void startTurn();

    /**
     * @brief Akcje przy rozpoczęciu tury.
     */
    void startTurnActions();


    /**
     * @brief Akcja zagrania karty.
     * @param card_
     * @param target_
     */
    void activePlayerPlayCard(Card* card_, Card* target_);


    /**
     * @brief Atak na kreaturę.
     * @param attacker
     * @param defender
     */
    void attackCreature(Card* attacker, Card* defender);

    /**
     * @brief Atak na gracza.
     * @param attacker
     */
    void attackPlayer(Card* attacker);

    /**
     * @brief Akcja poddania sie.
     */
    void activePlayerSurrender();

    /**
     * @brief Koniec tury.
     */
    void endTurn();

    /**
     * @brief Akcje na zakończenie tury.
     */
    void endTurnActions();

    /**
     * @brief Zakończenie gry.
     */
    void endGame();

    Game* getGame();
};

#endif // GAMEMANAGER_H
