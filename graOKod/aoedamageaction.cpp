#include "aoedamageaction.h"



AoeDamageAction::AoeDamageAction(Player *player_, int damage_) {
    player = player_;
    damage = damage_;
}

int AoeDamageAction::getDamage() {
    return damage;
}

Player *AoeDamageAction::getPlayer() {
    return player;
}

