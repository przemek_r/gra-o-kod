#include "deck.h"
#include "card.h"

Deck::Deck() {
   cards = CardVector();
}

Deck::Deck(CardVector list) {
    cards = list;
}

Deck::~Deck() {
    for (CardVector::iterator it = cards.begin(); it != cards.end(); ++it) {
        delete (*it);
    }
}

CardVector& Deck::getCards() {
    return cards;
}

void Deck::addCard(Card* card) {
    cards.push_back(card);
}

