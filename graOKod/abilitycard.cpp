#include "abilitycard.h"
#include "actionmanager.h"
#include "dealdamagetocreatureaction.h"
#include "noselectedcardexception.h"
#include "dealdamagetoplayeraction.h"
#include "game.h"
#include "destroycardaction.h"
#include "aoehealaction.h"
#include "addmanaaction.h"
#include "aoedecreaseattackaction.h"
#include "decreaseattackaction.h"
#include "readycardaction.h"

AbilityCard1::AbilityCard1(int id_) {
    id = id_;
    setId = 1;
    name = "Wyciek pamięci";
    attack = -1;
    health = -1;
    cost = 3;
    exhausted = false;
    desctription = "Zadaj 3 obrażenia przeciwnikowi";
}

void AbilityCard1::putAction(ActionManager *actionManager, Card *target) {
    actionManager->addAction(new DealDamageToPlayerAction(3, actionManager->getGame()->getNonActivePlayer()));
}

AbilityCard2::AbilityCard2(int id_) {
    id = id_;
    setId = 2;
    name = "Destruktor";
    attack = -1;
    health = -1;
    cost = 4;
    exhausted = false;
    desctription = "Zniszcz wybraną kreaturę";
}

void AbilityCard2::putAction(ActionManager *actionManager, Card *target) {
    if(target == nullptr) {
        throw NoSelectedCardException();
    }

    actionManager->addAction(new DestroyCardAction(target));
}

AbilityCard3::AbilityCard3(int id_) {
    id = id_;
    setId = 3;
    name = "Debugger";
    attack = -1;
    health = -1;
    cost = 3;
    exhausted = false;
    desctription = "Ulecz wszystkie przyjazne kreatury i gracza o 1";
}

void AbilityCard3::putAction(ActionManager *actionManager, Card *target) {
    actionManager->addAction(new AoeHealAction(1));
}

AbilityCard10::AbilityCard10(int id_) {
    id = id_;
    setId = 10;
    name = "Optymalizacja";
    attack = -1;
    health = -1;
    cost = 0;
    exhausted = false;
    desctription = "Dodaj 2 many";
}

void AbilityCard10::putAction(ActionManager *actionManager, Card *target) {
    actionManager->addAction(new AddManaAction(actionManager->getGame()->getActivePlayer(), 2));
}


AbilityCard16::AbilityCard16(int id_) {
    id = id_;
    setId = 16;
    name = "Spaghetti code";
    attack = -1;
    health = -1;
    cost = 3;
    exhausted = false;
    desctription = "Zmniejsza atak kreatur przeciwnika o 1";
}

void AbilityCard16::putAction(ActionManager *actionManager, Card *target) {
    actionManager->addAction(new AoeDecreaseAttackAction(1));
}


AbilityCard18::AbilityCard18(int id_) {
    id = id_;
    setId = 18;
    name = "Przełączenie kontekstu ";
    attack = -1;
    health = -1;
    cost = 3;
    exhausted = false;
    desctription = "Aktywuj wybraną karte";
}

void AbilityCard18::putAction(ActionManager *actionManager, Card *target) {
    if(target == nullptr) {
        throw NoSelectedCardException();
    }
    actionManager->addAction(new ReadyCardAction(target));
}


AbilityCard21::AbilityCard21(int id_) {
    id = id_;
    setId = 21;
    name = "U mnie działa";
    attack = -1;
    health = -1;
    cost = 3;
    exhausted = false;
    desctription = "Obniża atak wybranej kreatury o 4";
}

void AbilityCard21::putAction(ActionManager *actionManager, Card *target) {
    if(target == nullptr) {
        throw NoSelectedCardException();
    }

    actionManager->addAction(new DecreaseAttackAction(target, 4));
}
