#ifndef CARDMANAGER_H
#define CARDMANAGER_H

class Card;
class Deck;
class Player;
class Game;

/**
 * @brief Klasa obługuje operacje przemieszczania kart pomiędzy taliami.
 *
 */
class cardManager {
private:
    cardManager() {}

public:

    /**
     * @brief Dodaje karte do talii.
     * @param card_
     * @param to_
     */
    static void moveCard(Card* card_, Deck* to_);

    /**
     * @brief Przemiescza kartę z jednej talii do drugiej.
     * @param card_
     * @param from_
     * @param to_
     */
    static void moveCard(Card* card_, Deck* from_, Deck* to_);

    /**
     * @brief Zwraca właściciela karty.
     * @param card_
     * @param game_
     * @return
     */
    static Player* getCardOwner(Card* card_, Game* game_);

    /**
     * @brief Zwraca kartę o szukanym Id.
     * @param card_
     * @param game_
     * @return
     */
    static Card* getCardById(int id_, Game* game_);
};

#endif // CARDMANAGER_H
