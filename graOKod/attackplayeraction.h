#ifndef ATTACKPLAYERACTION_H
#define ATTACKPLAYERACTION_H
#include "action.h"


class Card;

/**
 * @brief Klasa przechowuje informacje o akcji 'atak na gracza'.
 *
 */
class AttackPlayerAction : public Action {
private:
    Card* attacker;

public:
    /**
     * @brief Konstruktor AttackPlayerAction
     * @param attacker_
     */
    AttackPlayerAction(Card* attacker_);
    ~AttackPlayerAction() {}

    Card* getAttacker();
};

#endif // ATTACKPLAYERACTION_H
