#ifndef ELUSIVECREATURECARD_H
#define ELUSIVECREATURECARD_H
#include "creaturecard.h"



class ElusiveCreatureCard : public CreatureCard {
public:
    ElusiveCreatureCard() {}
    ~ElusiveCreatureCard() {}

    bool isElusive();
};

class ElusiveCreatureCard6 : public ElusiveCreatureCard {
public:
    /**
     * @brief Konstruktor ElusiveCreatureCard6
     * @param id_
     */
    ElusiveCreatureCard6(int id_);
    ~ElusiveCreatureCard6() {}
};

class ElusiveCreatureCard20 : public ElusiveCreatureCard {
public:
    /**
     * @brief Konstruktor ElusiveCreatureCard20
     * @param id_
     */
    ElusiveCreatureCard20(int id_);
    ~ElusiveCreatureCard20() {}

    void putAction(ActionManager* actionManager, Card* target);
};

#endif // ELUSIVECREATURECARD_H
