#include "attackcreatureaction.h"

AttackCreatureAction::AttackCreatureAction(Card *attacker_, Card *defender_) {
    attacker = attacker_;
    defender = defender_;
}

Card *AttackCreatureAction::getAttacker() {
    return attacker;
}

Card *AttackCreatureAction::getDefender() {
    return defender;
}
