#include "playcardaction.h"

PlayCardAction::PlayCardAction(Card *playedCard_, Card *target_)
{

    playedCard = playedCard_;
    target = target_;
}

Card *PlayCardAction::getPlayedCard()
{
    return playedCard;
}

Card *PlayCardAction::getTarget()
{
    return target;
}
