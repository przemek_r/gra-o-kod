#ifndef ACTIONMANAGER_H
#define ACTIONMANAGER_H
#include <deque>
#include <vector>

class Action;
class ActionHandler;
class Game;

/**
 * @brief Klasa służąca do obslugi akcji. Pozwala na ich dodawanie, czyszczenie i wykonanie.
 *
 */
class ActionManager {
private:
    Game* game;
    std::deque<Action*> actionDeque;
    std::vector<ActionHandler*> actionHandlerVector;
public:
    /**
     * @brief Konstruktor ActionManager
     * @param game_
     */
    ActionManager(Game* game_);
    ~ActionManager();

    /**
     * @brief Dodaje akcje do łańcucha.
     * @param action_
     */
    void addAction(Action* action_);

    /**
     * @brief Wykonuje wszystkie akcje z łańcucha.
     */
    void resolveActions();

    /**
     * @brief Czyści łańcuch.
     */
    void clearActionDeque();

    Game* getGame();

};

#endif // ACTIONMANAGER_H
