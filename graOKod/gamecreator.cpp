#include "gamecreator.h"
#include "game.h"
#include "stackdeckcreator.h"


Game *GameCreator::createTestGame() {
    int id = 0;
    Player* p1 = new Player(StackDeckCreator::generateTestDeck(),1);
    Player* p2 = new Player(StackDeckCreator::generateTestDeck(),2);

    return new Game(id, p1, p2);
}

Game *GameCreator::createHotSeats(DeckType deckType) {
    int id = 0;
    Player* p1;
    Player* p2;

    if (deckType == DefaultDeck) {
         p1 = new Player(StackDeckCreator::generateTestDeck(),1);
         p2 = new Player(StackDeckCreator::generateTestDeck(),2);
    } else if (deckType == CustomDeck) {
        p1 = new Player(StackDeckCreator::loadDeckFromFile(),1);
        p2 = new Player(StackDeckCreator::loadDeckFromFile(100),2);
    }

    return new Game(id, p1, p2);
}

Game *GameCreator::createMultiplayer(DeckType deckType) {
    int id = 0;
    Player* p1;

    if (deckType == DefaultDeck) {
        p1 = new Player(StackDeckCreator::generateTestDeck(),0);
    } else if (deckType == CustomDeck) {
        p1 = new Player(StackDeckCreator::loadDeckFromFile(),0);
    }

    return new Game(id, p1, nullptr);
}

Game *GameCreator::createJoinMultiplayer(DeckType deckType) {
    int id = 0;
    Player* p2;

    if (deckType == DefaultDeck) {
        p2 = new Player(StackDeckCreator::generateTestDeck(100),0);
    } else if (deckType == CustomDeck) {
        p2 = new Player(StackDeckCreator::loadDeckFromFile(100),0);
    }
    return new Game(id, nullptr, p2);
}

