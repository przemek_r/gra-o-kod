#include "player.h"
#include "card.h"
#include "cardmanager.h"
#include "deck.h"
#include <string>

Player::Player() {

}

Player::Player(Deck *deck, int id_) {
    id = id_;
    handDeck = new Deck();
    tableDeck = new Deck();
    removedDeck = new Deck();
    stackDeck = deck;
    mana = 0;
    health = 20;
}

Player::~Player() {
    delete handDeck;
    delete tableDeck;
    delete removedDeck;
    delete stackDeck;
}

Deck *Player::getHandDeck() {
    return handDeck;
}

Deck *Player::getTableDeck() {
    return tableDeck;
}

Deck *Player::getStackDeck() {
    return stackDeck;
}

Deck *Player::getRemovedDeck() {
    return removedDeck;
}

void Player::setId(int id_) {
    id = id_;
}

void Player::setMana(int mana_) {
    mana = mana_;
}

void Player::setName(std::string name_) {
    name = name_;
}

void Player::addMana(int mana_) {
    mana += mana_;
}

void Player::subtractMana(int mana_) {
    mana -= mana_;
}

void Player::increaseHealth(int val) {
    health += val;
}

void Player::decreaseHealth(int val) {
    health -= val;
}

int Player::getMana() {
    return mana;
}

int Player::getHealth() {
    return health;
}

int Player::getId() {
    return id;
}

std::string Player::getName() {
    return name;
}

