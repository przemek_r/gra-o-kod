#ifndef ATTACKEXCEPTION_H
#define ATTACKEXCEPTION_H
#include <exception>

/**
 * @brief Interfejs wyjątków wyrzucanych przy ataku.
 *  
 */
class AttackException : public std::exception {
public:
    virtual const char* what() const throw()=0;
};


/**
 * @brief Wyjątek ataku na kreaturę nietykalną.
 *  
 */
class ElusivnessException : public AttackException {
public:
    ElusivnessException() {}
    virtual const char* what() const throw();
};

/**
 * @brief Wyjątek namierzenia karty nienamierzalnej.
 *  
 */
class UntargetableException : public AttackException {
    public:
    UntargetableException() {}
    virtual const char* what() const throw();
};

/**
 * @brief Wyjątek ataku kartą wyczerpaną.
 *  
 */
class ExhaustedException : public AttackException {
    public:
    ExhaustedException() {}
    virtual const char* what() const throw();
};

/**
 * @brief Wyjątek ataku przy obecności prowokatorów.
 *  
 */
class ProtectorException : public AttackException {
    public:
    ProtectorException() {}
    virtual const char* what() const throw();
};

#endif // ATTACKEXCEPTION_H
