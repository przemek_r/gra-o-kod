#include "gamemanager.h"
#include "gameinitializer.h"
#include "game.h"
#include "player.h"
#include "card.h"
#include "actionmanager.h"
#include "drawcardaction.h"
#include "deckendexception.h"
#include "gamefinishexception.h"
#include "playcardaction.h"
#include "attackcreatureaction.h"
#include "attackplayeraction.h"
#include "readycardaction.h"
#include "noselectedcardexception.h"
#include "dealdamagetoplayeraction.h"
#include "checkplayersdeathaction.h"
#include <exception>


class GameInitializer;

GameManager::GameManager() {
    game = nullptr;
    actionManager = nullptr;
}

GameManager::~GameManager() {}

void GameManager::createGame(GameCreateMode gameCreateMode) {
    if (gameCreateMode == HotSeats) {
        game = GameInitializer::initGame();
    } else if(gameCreateMode == CreateMultiplayer) {
        game = GameInitializer::initMultiplayerGame();
    } else if(gameCreateMode == JoinMultiplayer) {
        game = GameInitializer::initJoinMultiplayerGame();
    }
    actionManager = new ActionManager(game);
}

void GameManager::joinGame(
    GameCreateMode gameCreateMode,
    int opponentId,
    std::string opponentName,
    Deck *opponentDeck,
    int gameId,
    int playerId
) {
    if (gameCreateMode == JoinMultiplayer) {
        game->setId(gameId);
        game->setPlayerOne(new Player(opponentDeck, opponentId));
        game->getPlayerOne()->setName(opponentName);
        game->getLocalPlayer()->setId(playerId);
        game->initActivePlayer();
    } else if (gameCreateMode == CreateMultiplayer) {
        game->setPlayerTwo(new Player(opponentDeck, opponentId));
        game->getPlayerTwo()->setName(opponentName);
        game->initActivePlayer();
    }
}

void GameManager::startGame() {
    // 3 karty dla każdego gracza przez ActionManager
    for (int i = 0; i < 3; ++i) {
        actionManager->addAction(new DrawCardAction(game->getPlayerOne()));
        actionManager->addAction(new DrawCardAction(game->getPlayerTwo()));
    }
    actionManager->resolveActions();
}

void GameManager::startTurn() {
    if (game->getActivePlayer() == game->getPlayerOne()) { // jesli aktywny jest gracz pierwszy, to inkrementuje licznik tur
        game->currentTurnNumberIncrement();
    }
    game->getActivePlayer()->setMana(game->getCurrentTurnNumber()); //ustaw mane gracza

    activePlayerDrawCard();

}

void GameManager::startTurnActions() {
    for (
        CardVector::iterator it = game->getActivePlayer()->getTableDeck()->getCards().begin();
        it != game->getActivePlayer()->getTableDeck()->getCards().end();
        ++it
    ){
        actionManager->addAction(new ReadyCardAction((*it)));
        (*it)->atTurnStartAction(actionManager);
        actionManager->resolveActions();
    }
}

void GameManager::activePlayerDrawCard() {
    actionManager->addAction(new DrawCardAction(game->getActivePlayer()));

    try {
        actionManager->resolveActions();
    } catch (DeckEndException& ex){
        if (ex.getPlayer()->getId() == 1) {
            throw GameFinishException(2, game->getPlayerTwo()->getName());
        }

        throw GameFinishException(1, game->getPlayerOne()->getName());
    }
}

void GameManager::activePlayerPlayCard(Card *card_, Card *target_) {
    if (card_ == nullptr) {
        throw NoSelectedCardException();
    }

    actionManager->addAction(new PlayCardAction(card_, target_));
    actionManager->resolveActions();
}

void GameManager::attackCreature(Card *attacker, Card *defender) {
    if (attacker == nullptr || defender == nullptr) {
        throw NoSelectedCardException();
    }

    actionManager->addAction(new AttackCreatureAction(attacker, defender));
    actionManager->resolveActions();
}

void GameManager::attackPlayer(Card *attacker) {
    if (attacker == nullptr) {
        throw NoSelectedCardException();
    }

    actionManager->addAction(new AttackPlayerAction(attacker));
    actionManager->resolveActions();
}

void GameManager::activePlayerSurrender() {
    actionManager->addAction(new DealDamageToPlayerAction(game->getActivePlayer()->getHealth(), game->getActivePlayer()));
    actionManager->addAction(new CheckPlayersDeathAction);
    actionManager->resolveActions();
}

void GameManager::endTurn() {
    game->changeActivePlayer();
}

void GameManager::endTurnActions() {
    for(
        CardVector::iterator it = game->getActivePlayer()->getTableDeck()->getCards().begin();
        it != game->getActivePlayer()->getTableDeck()->getCards().end();
        ++it
    ){
        (*it)->atTurnEndAction(actionManager);
        actionManager->resolveActions();
    }
}

void GameManager::endGame() {
    delete game;
    delete actionManager;
}

Game *GameManager::getGame() {
    return game;
}

