#ifndef READYCARDACTION_H
#define READYCARDACTION_H
#include "action.h"

class Card;

/**
 * @brief Klasa przechowuje informacje o akcji 'przygotowanie karty'.
 *
 */
class ReadyCardAction : public Action {
private:
    Card* target;
public:
    /**
     * @brief Konstruktor ReadyCardAction
     * @param target_
     */
    ReadyCardAction(Card* target_);
    ~ReadyCardAction() {}

    Card* getTarget();
};

#endif // READYCARDACTION_H
