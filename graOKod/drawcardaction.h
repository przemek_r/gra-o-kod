#ifndef DRAWCARDACTION_H
#define DRAWCARDACTION_H
#include "action.h"

class Player;


/**
 * @brief Klasa przechowuje informacje o akcji 'pociągnięcie karty'.
 *
 */
class DrawCardAction : public Action {
private:
    Player* player;

public:
    /**
     * @brief Konstruktor DrawCardAction
     * @param player_
     */
    DrawCardAction(Player* player_);
    ~DrawCardAction() {}

    Player* getPlayer();
};

#endif // DRAWCARDACTION_H
