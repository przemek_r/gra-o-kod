#ifndef STEALTHCREATURECARD_H
#define STEALTHCREATURECARD_H
#include "creaturecard.h"

class StealthCreatureCard : public CreatureCard {
public:
    StealthCreatureCard() {}
    ~StealthCreatureCard() {}

    bool isStealth();
};

class StealthCreatureCard1 : public StealthCreatureCard {
public:
    /**
     * @brief Konstruktor StealthCreatureCard1
     * @param id_
     */
    StealthCreatureCard1(int id_);
    ~StealthCreatureCard1() {}
};

class StealthCreatureCard2 : public StealthCreatureCard {
public:
    /**
     * @brief Kontruktor StealthCreatureCard2
     * @param id_
     */
    StealthCreatureCard2(int id_);
    ~StealthCreatureCard2() {}
};

#endif // STEALTHCREATURECARD_H
