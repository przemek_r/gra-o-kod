#ifndef DEALDAMAGETOPLAYERACTION_H
#define DEALDAMAGETOPLAYERACTION_H
#include "action.h"

class Player;

/**
 * @brief Klasa przechowuje informacje o akcji 'zadanie obrażeń graczowi'.
 *
 */
class DealDamageToPlayerAction : public Action{
private:
    int damage;
    Player* player;
public:
    /**
     * @brief Konstruktor DealDamageToPlayerAction
     * @param value_
     * @param player_
     */
    DealDamageToPlayerAction(int value_, Player* player_);
    DealDamageToPlayerAction() {}

    int getDamage();
    Player* getPlayer();
};

#endif // DEALDAMAGETOPLAYERACTION_H
