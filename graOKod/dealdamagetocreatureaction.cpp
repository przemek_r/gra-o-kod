#include "dealdamagetocreatureaction.h"

DealDamageToCreatureAction::DealDamageToCreatureAction(int damage_, Card *card_) {

    damage = damage_;
    card = card_;
}

int DealDamageToCreatureAction::getDamage() {
    return damage;
}

Card *DealDamageToCreatureAction::getCard() {
    return card;
}

