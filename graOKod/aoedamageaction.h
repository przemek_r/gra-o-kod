#ifndef AOEDAMAGEACTION_H
#define AOEDAMAGEACTION_H
#include "action.h"

class Player;

/**
 * @brief Klasa przechowuje informacje o akcji 'obrażenia obszarowe'.
 *
 */
class AoeDamageAction : public Action {
private:
    int damage;
    Player* player;

public:
    /**
     * @brief Konstruktor AoeDamageAction
     * @param player_
     * @param damage_
     */
    AoeDamageAction(Player* player_, int damage_);
    ~AoeDamageAction() {}

    int getDamage();
    Player* getPlayer();
};

#endif // AOEDAMAGEACTION_H

