#ifndef CHECKCASUALITIESACTION_H
#define CHECKCASUALITIESACTION_H
#include "action.h"

/**
 * @brief Klasa przechowuje informacje o akcji 'sprawdzenie śmierci kreatur'.
 *
 */
class CheckCasualtiesAction : public Action {
public:
    /**
     * @brief Konstruktor CheckCasualtiesAction
     */
    CheckCasualtiesAction();
    ~CheckCasualtiesAction() {}
};

#endif // CHECKCASUALITIESACTION_H
