#include "healcreatureaction.h"


HealCreatureAction::HealCreatureAction(int value_, Card *card_) {
    heal = value_;
    card = card_;
}

int HealCreatureAction::getHeal() {
    return heal;
}

Card *HealCreatureAction::getCard() {
    return card;
}

