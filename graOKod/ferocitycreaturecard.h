#ifndef FEROCITYCREATURECARD_H
#define FEROCITYCREATURECARD_H
#include "creaturecard.h"

class FerocityCreatureCard : public CreatureCard {
public:
    FerocityCreatureCard() {}
    ~FerocityCreatureCard() {}

    bool isFerocity();
};

class FerocityCreatureCard7 : public FerocityCreatureCard {
public:
    /**
     * @brief Konstruktor FerocityCreatureCard7
     * @param id_
     */
    FerocityCreatureCard7(int id_);
    ~FerocityCreatureCard7() {}

    void attackAction(ActionManager* ActionManager);
};

class FerocityCreatureCard15 : public FerocityCreatureCard {
public:
    /**
     * @brief Konstruktor FerocityCreatureCard15
     * @param id_
     */
    FerocityCreatureCard15(int id_);
    ~FerocityCreatureCard15() {}
};

#endif // FEROCITYCREATURECARD_H
