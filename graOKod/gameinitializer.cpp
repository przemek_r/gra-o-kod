#include "gameinitializer.h"
#include "gamecreator.h"

Game *GameInitializer::initGame() {
    return GameCreator::createHotSeats(CustomDeck);
}

Game *GameInitializer::initMultiplayerGame() {
    return GameCreator::createMultiplayer(CustomDeck);
}

Game *GameInitializer::initJoinMultiplayerGame() {
    return GameCreator::createJoinMultiplayer(CustomDeck);
}
