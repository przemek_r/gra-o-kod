#ifndef PROTECTORCREATURECARD_H
#define PROTECTORCREATURECARD_H
#include "creaturecard.h"

class ActionManager;

class ProtectorCreatureCard : public CreatureCard {
public:
    ProtectorCreatureCard() {}
    ~ProtectorCreatureCard() {}

    bool isProtector();
};

class ProtectorCreatureCard4 : public ProtectorCreatureCard {
public:
    /**
     * @brief Konstruktor ProtectorCreatureCard4
     * @param id_
     */
    ProtectorCreatureCard4(int id_);
    ~ProtectorCreatureCard4() {}

    void atTurnStartAction(ActionManager* actionManager);
};

class ProtectorCreatureCard13 : public ProtectorCreatureCard {
public:
    /**
     * @brief Konstruktor ProtectorCreatureCard13
     * @param id_
     */
    ProtectorCreatureCard13(int id_);
    ~ProtectorCreatureCard13() {}
};

class ProtectorCreatureCard17 : public ProtectorCreatureCard {
public:
    /**
     * @brief Konstruktor ProtectorCreatureCard17
     * @param id_
     */
    ProtectorCreatureCard17(int id_);
    ~ProtectorCreatureCard17() {}
};

class ProtectorCreatureCard19 : public ProtectorCreatureCard {
public:
    /**
     * @brief Konstruktor ProtectorCreatureCard19
     * @param id_
     */
    ProtectorCreatureCard19(int id_);
    ~ProtectorCreatureCard19() {}

    void destroyedAction(ActionManager* actionManager);
};

#endif // PROTECTORCREATURECARD_H
