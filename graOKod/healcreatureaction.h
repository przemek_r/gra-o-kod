#ifndef HEALCREATUREACTION_H
#define HEALCREATUREACTION_H
#include "action.h"

class Card;

/**
 * @brief Klasa przechowuje informacje o akcji 'uleczenie kreatury'.
 *
 */
class HealCreatureAction : public Action {
private:
    Card* card;
    int heal;

public:
    /**
     * @brief Konstruktor HealCreatureAction
     * @param value_
     * @param card_
     */
    HealCreatureAction(int value_, Card* card_);
    ~HealCreatureAction() {}

    int getHeal();
    Card* getCard();
};

#endif // HEALCREATUREACTION_H
