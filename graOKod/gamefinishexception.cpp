#include "gamefinishexception.h"

GameFinishException::GameFinishException(int id, std::string name_) {
    winnerId = id;
    name = name_;
}

const char *GameFinishException::what() const throw() {
    return "Koniec gry";
}

std::string GameFinishException::winner() {
    if (winnerId) {
        return "Wygrywa gracz " + std::to_string(winnerId) + "!";
    }

    return "Remis!";
}

std::string GameFinishException::winnerName() {
    if (winnerId) {
        return "Wygrywa " + name + "!";
    }

    return "Remis!";
}

