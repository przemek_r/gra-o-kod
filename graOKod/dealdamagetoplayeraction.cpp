#include "dealdamagetoplayeraction.h"



DealDamageToPlayerAction::DealDamageToPlayerAction(int value_, Player *player_) {
    damage = value_;
    player = player_;
}

int DealDamageToPlayerAction::getDamage() {
    return damage;
}

Player *DealDamageToPlayerAction::getPlayer() {
    return player;
}
