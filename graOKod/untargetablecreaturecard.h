#ifndef UNTARGETABLECREATURECARD_H
#define UNTARGETABLECREATURECARD_H
#include "creaturecard.h"

class UntargetableCreatureCard : public CreatureCard {
public:
    UntargetableCreatureCard() {}
    ~UntargetableCreatureCard() {}

    bool isUntargetable();
};

class UntargetableCreatureCard14 : public UntargetableCreatureCard {
public:
    /**
     * @brief Konstruktor UntargetableCreatureCard14
     * @param id_
     */
    UntargetableCreatureCard14(int id_);
    ~UntargetableCreatureCard14() {}
};


#endif // UNTARGETABLECREATURECARD_H
