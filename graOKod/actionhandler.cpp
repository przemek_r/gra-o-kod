#include <typeinfo>
#include "actionhandler.h"
#include "actionmanager.h"
#include <iostream>
#include "drawcardaction.h"
#include "player.h"
#include "cardmanager.h"
#include "deckendexception.h"
#include "game.h"
#include "playcardaction.h"
#include "card.h"
#include "notenoughtmanaexception.h"
#include "checkcasualitiesaction.h"
#include "checkplayersdeathaction.h"
#include "gamefinishexception.h"
#include "attackcreatureaction.h"
#include "attackexception.h"
#include "dealdamagetocreatureaction.h"
#include "attackplayeraction.h"
#include "dealdamagetoplayeraction.h"
#include "healcreatureaction.h"
#include "healplayeraction.h"
#include "aoehealaction.h"
#include "aoedamageaction.h"
#include "readycardaction.h"
#include "destroycardaction.h"
#include "addmanaaction.h"
#include "increaseattackaction.h"
#include "decreaseattackaction.h"
#include "aoeincreaseattackaction.h"
#include "aoedecreaseattackaction.h"

using namespace std;

ActionHandler::ActionHandler(ActionManager *actionManager_, Game *game_) {
    next = nullptr;
    actionManager = actionManager_;
    game = game_;
}

void ActionHandler::addHandler(ActionHandler *nextHandler_)
{
    if(next) {
        next->addHandler(nextHandler_);
    } else {
        next = nextHandler_;
    }
}

void DrawCardActionHandler::handle(Action *action_)
{
    if(typeid(*action_)==(typeid(DrawCardAction))){
        DrawCardAction* drawCardAction = dynamic_cast<DrawCardAction*>(action_);

        //sprawdzenie czy talia nie jest pusta
        if(drawCardAction->getPlayer()->getStackDeck()->getCards().empty()){
            throw DeckEndException(drawCardAction->getPlayer());
        }

        Card* topStackCard = drawCardAction->getPlayer()->getStackDeck()->getCards().back();
        drawCardAction->getPlayer()->getStackDeck()->getCards().pop_back();
        cardManager::moveCard(topStackCard, drawCardAction->getPlayer()->getHandDeck());

        delete drawCardAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void PlayCardActionHandler::handle(Action *action_)
{
    if(typeid(*action_)==(typeid(PlayCardAction))){

        PlayCardAction* playCardAction = dynamic_cast<PlayCardAction*>(action_);

        //sprawdzenie many
        if(game->getActivePlayer()->getMana() < playCardAction->getPlayedCard()->getCost()) {
            actionManager->clearActionDeque();
            throw NotEnoughtManaException();
        }


        //sprawdzenie untargetable
        if(playCardAction->getTarget() != nullptr) {
            if(playCardAction->getTarget()->isUntargetable()) {
                actionManager->clearActionDeque();
                throw UntargetableException();
            }
        }

        //efekty karty
        playCardAction->getPlayedCard()->putAction(actionManager, playCardAction->getTarget());

        //odjecie many
        game->getActivePlayer()->subtractMana(playCardAction->getPlayedCard()->getCost());

        //wystawienie karty
        cardManager::moveCard(playCardAction->getPlayedCard(),
                              game->getActivePlayer()->getHandDeck(),
                              game->getActivePlayer()->getTableDeck());

        //ustawienie wyczerpania
        if (!playCardAction->getPlayedCard()->isFerocity()) {
            playCardAction->getPlayedCard()->setExhausted(true);
        }

        //sprawdzenie smierci kart
        actionManager->addAction(new CheckCasualtiesAction);

        //sprawdzenie smierci graczy
        actionManager->addAction(new CheckPlayersDeathAction);

        delete playCardAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void CheckCasualtiesActionHandler::handle(Action *action_) {

    if(typeid(*action_)==(typeid(CheckCasualtiesAction))) {
        CheckCasualtiesAction* checkCasualtiesAction = dynamic_cast<CheckCasualtiesAction*>(action_);

        bool removeFlag = true;

        while(removeFlag) {
            removeFlag = false;

            for( CardVector::iterator it = game->getPlayerOne()->getTableDeck()->getCards().begin();
                it != game->getPlayerOne()->getTableDeck()->getCards().end();
                ++it) {
                if ( (*it)->getHealth() <= 0 ){
                    cardManager::moveCard(*it,
                                          game->getPlayerOne()->getTableDeck(),
                                          game->getPlayerOne()->getRemovedDeck());
                    (*it)->destroyedAction(actionManager);
                    removeFlag = true;
                    break;
                }
            }

            for( CardVector::iterator it = game->getPlayerTwo()->getTableDeck()->getCards().begin();
                it != game->getPlayerTwo()->getTableDeck()->getCards().end();
                ++it) {
                if ( (*it)->getHealth() <= 0 ){
                    cardManager::moveCard(*it,
                                          game->getPlayerTwo()->getTableDeck(),
                                          game->getPlayerTwo()->getRemovedDeck());
                    (*it)->destroyedAction(actionManager);
                    removeFlag = true;
                    break;
                }
            }
        }
        delete checkCasualtiesAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void CheckPlayersDeathActionHandler::handle(Action *action_)
{
    if(typeid(*action_)==(typeid(CheckPlayersDeathAction))) {
        CheckPlayersDeathAction* checkPlayersDeathAction = dynamic_cast<CheckPlayersDeathAction*>(action_);
        int player1Hp = game->getPlayerOne()->getHealth();
        int player2Hp = game->getPlayerTwo()->getHealth();
        if (player1Hp <= 0 && player2Hp <= 0){
            delete checkPlayersDeathAction;
            throw GameFinishException(0, "");
        } else if ( player1Hp <= 0) {
            delete checkPlayersDeathAction;
            throw GameFinishException(2, game->getPlayerTwo()->getName());
        } else if ( player2Hp <= 0) {
            delete checkPlayersDeathAction;
            throw GameFinishException(1, game->getPlayerOne()->getName());
        }
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void AttackCreatureActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(AttackCreatureAction))) {
        AttackCreatureAction* attackCreatureAction = dynamic_cast<AttackCreatureAction*>(action_);

        //sprawdz exhausted
        if(attackCreatureAction->getAttacker()->isExhausted()) {
            actionManager->clearActionDeque();
            throw ExhaustedException();
        }

        //sprawdz ellusive
        if(attackCreatureAction->getDefender()->isElusive()) {
            actionManager->clearActionDeque();
            throw ElusivnessException();
        }

        //sprawdz protektorów
        if(!attackCreatureAction->getDefender()->isProtector()) {
            if(!attackCreatureAction->getAttacker()->isStealth()) {
                for( CardVector::iterator it = game->getNonActivePlayer()->getTableDeck()->getCards().begin();
                      it != game->getNonActivePlayer()->getTableDeck()->getCards().end();
                      ++it) {
                    if ((*it)->isProtector()){
                        actionManager->clearActionDeque();
                        throw ProtectorException();
                    }
                }
            }
        }

        //akcja przy ataku
        attackCreatureAction->getAttacker()->attackAction(actionManager);

        // wyczerpanie
        attackCreatureAction->getAttacker()->setExhausted(true);

        //zadaj dmg
        actionManager->addAction(new DealDamageToCreatureAction(attackCreatureAction->getAttacker()->getAttack(),
                                                                attackCreatureAction->getDefender()));
        actionManager->addAction(new DealDamageToCreatureAction(attackCreatureAction->getDefender()->getAttack(),
                                                                attackCreatureAction->getAttacker()));

        //sprawdz smierci
        actionManager->addAction(new CheckCasualtiesAction);

        //sprawdzenie smierci graczy
        actionManager->addAction(new CheckPlayersDeathAction);


        delete attackCreatureAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void DealDamageToCreatureActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(DealDamageToCreatureAction))) {
        DealDamageToCreatureAction* dealDamageToCreatureAction = dynamic_cast<DealDamageToCreatureAction*>(action_);

        dealDamageToCreatureAction->getCard()->decreaseHealth(dealDamageToCreatureAction->getDamage());

        actionManager->addAction(new CheckCasualtiesAction);
        actionManager->addAction(new CheckPlayersDeathAction);

        delete dealDamageToCreatureAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }

}

void AttackPlayerActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(AttackPlayerAction))) {
        AttackPlayerAction* attackPlayerAction = dynamic_cast<AttackPlayerAction*>(action_);

        //sprawdz exhausted
        if(attackPlayerAction->getAttacker()->isExhausted()) {
            actionManager->clearActionDeque();
            throw ExhaustedException();
        }

        //sprawdz protektorów
        if(!attackPlayerAction->getAttacker()->isStealth()) {
            for( CardVector::iterator it = game->getNonActivePlayer()->getTableDeck()->getCards().begin();
                 it != game->getNonActivePlayer()->getTableDeck()->getCards().end();
                 ++it) {
                if ((*it)->isProtector()){
                    actionManager->clearActionDeque();
                    throw ProtectorException();
                }
            }
        }

        //akcja przy ataku
        attackPlayerAction->getAttacker()->attackAction(actionManager);

        // wyczerpanie
        attackPlayerAction->getAttacker()->setExhausted(true);

        //zadaj dmg
        actionManager->addAction(new DealDamageToPlayerAction(attackPlayerAction->getAttacker()->getAttack(),
                                                              game->getNonActivePlayer()));


        //sprawdz smierci
        actionManager->addAction(new CheckCasualtiesAction);

        //sprawdzenie smierci graczy
        actionManager->addAction(new CheckPlayersDeathAction);


        delete attackPlayerAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void DealDamageToPlayerActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(DealDamageToPlayerAction))) {
        DealDamageToPlayerAction* dealDamageToPlayerAction = dynamic_cast<DealDamageToPlayerAction*>(action_);

        dealDamageToPlayerAction->getPlayer()->decreaseHealth(dealDamageToPlayerAction->getDamage());

        actionManager->addAction(new CheckPlayersDeathAction);

        delete dealDamageToPlayerAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }

}

void HealCreatureActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(HealCreatureAction))) {
        HealCreatureAction* healCreatureAction = dynamic_cast<HealCreatureAction*>(action_);

        healCreatureAction->getCard()->increaseHealth(healCreatureAction->getHeal());

        delete healCreatureAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void HealPlayerActionHandler::handle(Action *action_)
{
    if(typeid(*action_)==(typeid(HealPlayerAction))) {
        HealPlayerAction* healPlayerAction = dynamic_cast<HealPlayerAction*>(action_);

        healPlayerAction->getPlayer()->increaseHealth(healPlayerAction->getHeal());

        delete healPlayerAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void AoeHealActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(AoeHealAction))) {
        AoeHealAction* aoeHealAction = dynamic_cast<AoeHealAction*>(action_);

        //ulczenie gracza
        actionManager->addAction(new HealPlayerAction(aoeHealAction->getHeal(),
                                                      game->getActivePlayer()));

        //uleczenie kreatur
        for(CardVector::iterator it = game->getActivePlayer()->getTableDeck()->getCards().begin();
            it != game->getActivePlayer()->getTableDeck()->getCards().end();
            ++it){
            actionManager->addAction(new HealCreatureAction(aoeHealAction->getHeal(),
                                                            (*it)));
        }

        delete aoeHealAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void AoeDamageActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(AoeDamageAction))) {
        AoeDamageAction* aoeDamageAction = dynamic_cast<AoeDamageAction*>(action_);

        //ulczenie gracza
        actionManager->addAction(new DealDamageToPlayerAction(aoeDamageAction->getDamage(),
                                                              game->getNonActivePlayer()));

        //uleczenie kreatur
        for(CardVector::iterator it = aoeDamageAction->getPlayer()->getTableDeck()->getCards().begin();
            it != aoeDamageAction->getPlayer()->getTableDeck()->getCards().end();
            ++it){
            actionManager->addAction(new DealDamageToCreatureAction(aoeDamageAction->getDamage(),
                                                                    (*it)));
        }

        delete aoeDamageAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void ReadyCardActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(ReadyCardAction))) {
        ReadyCardAction* readyCardAction = dynamic_cast<ReadyCardAction*>(action_);

        readyCardAction->getTarget()->setExhausted(false);

        delete readyCardAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void DestroyCardActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(DestroyCardAction))) {
        DestroyCardAction* destroyCardAction = dynamic_cast<DestroyCardAction*>(action_);

        destroyCardAction->getTarget()->decreaseHealth(destroyCardAction->getTarget()->getHealth());

        actionManager->addAction(new CheckCasualtiesAction);
        actionManager->addAction(new CheckPlayersDeathAction);

        delete destroyCardAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void AddManaActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(AddManaAction))) {
        AddManaAction* addManaAction = dynamic_cast<AddManaAction*>(action_);

        addManaAction->getPlayer()->addMana(addManaAction->getValue());

        delete addManaAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void IncreaseAttackActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(IncreaseAttackAction))) {
        IncreaseAttackAction* increaseAttackAction = dynamic_cast<IncreaseAttackAction*>(action_);

        increaseAttackAction->getTarget()->increaseAttack(increaseAttackAction->getValue());

        delete increaseAttackAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void DecreaseAttackActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(DecreaseAttackAction))) {
        DecreaseAttackAction* decreaseAttackAction = dynamic_cast<DecreaseAttackAction*>(action_);

        decreaseAttackAction->getTarget()->decreaseAttack(decreaseAttackAction->getValue());

        delete decreaseAttackAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}


void AoeIncreaseAttackActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(AoeIncreaseAttackAction))) {
        AoeIncreaseAttackAction* aoeIncreaseAttackAction = dynamic_cast<AoeIncreaseAttackAction*>(action_);

        for(CardVector::iterator it = game->getActivePlayer()->getTableDeck()->getCards().begin();
            it != game->getActivePlayer()->getTableDeck()->getCards().end();
            ++it){
            actionManager->addAction(new IncreaseAttackAction((*it),aoeIncreaseAttackAction->getValue()));
        }

        delete aoeIncreaseAttackAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}

void AoeDecreaseAttackActionHandler::handle(Action *action_) {
    if(typeid(*action_)==(typeid(AoeDecreaseAttackAction))) {
        AoeDecreaseAttackAction* aoeDecreaseAttackAction = dynamic_cast<AoeDecreaseAttackAction*>(action_);

        for(CardVector::iterator it = game->getNonActivePlayer()->getTableDeck()->getCards().begin();
            it != game->getNonActivePlayer()->getTableDeck()->getCards().end();
            ++it){
            actionManager->addAction(new DecreaseAttackAction((*it),aoeDecreaseAttackAction->getValue()));
        }

        delete aoeDecreaseAttackAction;
    } else if(next != nullptr) {
        next->handle(action_);
    }
}
