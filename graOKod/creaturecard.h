#ifndef CREATURECARD_H
#define CREATURECARD_H
#include "card.h"


/**
 * @brief Klasa przedstawiająca kreaturę.
 *
 */
class CreatureCard : public Card {
protected:
public:
    CreatureCard();
    ~CreatureCard();
};

#endif // CREATURECARD_H
