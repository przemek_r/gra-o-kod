#include "cardfactory.h"
#include "card.h"
#include "abilitycard.h"
#include "normalcreaturecard.h"
#include "elusivecreaturecard.h"
#include "ferocitycreaturecard.h"
#include "protectorcreaturecard.h"
#include "untargetablecreaturecard.h"
#include "stealthcreaturecard.h"


Card* CardFactory::createCardBySetId(int setId, int id) {
    if (setId == 1) return new AbilityCard1(id);
    if (setId == 2) return new AbilityCard2(id);
    if (setId == 3) return new AbilityCard3(id);
    if (setId == 4) return new ProtectorCreatureCard4(id);
    if (setId == 5) return new NormalCreatureCard5(id);
    if (setId == 6) return new ElusiveCreatureCard6(id);
    if (setId == 7) return new FerocityCreatureCard7(id);
    if (setId == 8) return new NormalCreatureCard8(id);
    if (setId == 9) return new NormalCreatureCard9(id);
    if (setId == 10) return new AbilityCard10(id);
    if (setId == 11) return new NormalCreatureCard11(id);
    if (setId == 12) return new NormalCreatureCard12(id);
    if (setId == 13) return new ProtectorCreatureCard13(id);
    if (setId == 14) return new UntargetableCreatureCard14(id);
    if (setId == 15) return new FerocityCreatureCard15(id);
    if (setId == 16) return new AbilityCard16(id);
    if (setId == 17) return new ProtectorCreatureCard17(id);
    if (setId == 18) return new AbilityCard18(id);
    if (setId == 19) return new ProtectorCreatureCard19(id);
    if (setId == 20) return new ElusiveCreatureCard20(id);
    if (setId == 21) return new AbilityCard21(id);
}

int CardFactory::getCardNumber() {
    return 21;
}
