#ifndef GAME_H
#define GAME_H
#include "player.h"

/**
 * @brief Klasa przechowujące podstawowe dane o grze takie jak:
 * - id
 * - gracz pierwszy
 * - gracz drugi
 * - aktywny gracz
 *
 */
class Game {
private:
    int id;
    Player* playerOne;
    Player* playerTwo;
    Player* activePlayer;
    Player* nonActivePlayer;
    Player* localPlayer;
    int currentTurnNumber;
public:

    /**
     * @brief Konstruktor Game
     * @param id_
     * @param p1_
     * @param p2_
     */
    Game(int id_, Player* p1_, Player* p2_ = nullptr);
    ~Game();

    Player* getPlayerOne();
    Player* getPlayerTwo();
    Player* getPlayer(int id);
    Player* getActivePlayer();
    Player* getNonActivePlayer();
    Player* getLocalPlayer();
    Player* getOtherPlayer(Player* player);
    int getId();
    int getCurrentTurnNumber();

    void setId(int _id);
    void setPlayerOne(Player* p1_);
    void setPlayerTwo(Player* p2_);
    void setLocalPlayer(Player* p_);

    /**
     * @brief Inkrementuje licznik tur.
     */
    void currentTurnNumberIncrement();

    /**
     * @brief Zmienia aktywnego gracza.
     */
    void changeActivePlayer();

    /**
     * @brief Inicjalizuje aktynwych graczy.
     */
    void initActivePlayer();
};

#endif // GAME_H
