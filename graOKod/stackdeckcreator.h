#ifndef STACKDECKCREATOR_H
#define STACKDECKCREATOR_H
#include "deck.h"
#include "../rapidjson/document.h"

/**
 * @brief Klasa tworząca talie.
 *  
 */
class StackDeckCreator {
private:
    StackDeckCreator() {}
public:
    /**
     * @brief generuje talie gracza.
     * @param cardCounter
     * @return
     */
    static Deck *generateTestDeck(int cardCounter = 1);

    /**
     * @brief Ładuje talie z pliku.
     * @param cardCounter
     * @return
     */
    static Deck *loadDeckFromFile(int cardCounter = 1);

    /**
     * @brief Tworzy talie na podstawie JSON.
     * @param deck
     * @return
     */
    static Deck* createFromJson(rapidjson::Value& deck);
};

#endif // STACKDECKCREATOR_H
