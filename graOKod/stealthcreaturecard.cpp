#include "stealthcreaturecard.h"

bool StealthCreatureCard::isStealth() {
    return true;
}

StealthCreatureCard1::StealthCreatureCard1(int id_) {
    name = "";
    setId = 11;
    id = id_;
    attack = 3;
    health = 3;
    cost = 4;
    exhausted = false;
    desctription = "UKRYCIE";
}

StealthCreatureCard2::StealthCreatureCard2(int id_) {
    name = "";
    setId = 12;
    id = id_;
    attack = 2;
    health = 1;
    cost = 2;
    exhausted = false;
    desctription = "UKRYCIE";
}
