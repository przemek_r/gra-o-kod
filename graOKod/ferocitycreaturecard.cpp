#include "ferocitycreaturecard.h"
#include "actionmanager.h"
#include "increaseattackaction.h"


bool FerocityCreatureCard::isFerocity() {
    return true;
}

FerocityCreatureCard7::FerocityCreatureCard7(int id_) {
    id = id_;
    setId = 7;
    name = "Iterator";
    attack = 1;
    health = 3;
    cost = 3;
    exhausted = false;
    desctription = "SZARŻA\n Każda akcja ataku Iteratora zwiększa jego atak o 1";
}

void FerocityCreatureCard7::attackAction(ActionManager *ActionManager) {
    ActionManager->addAction(new IncreaseAttackAction(this, 1));
}

FerocityCreatureCard15::FerocityCreatureCard15(int id_) {
    id = id_;
    setId = 15;
    name = "RTOS";
    attack = 3;
    health = 1;
    cost = 3;
    exhausted = false;
    desctription = "SZARŻA";

}
