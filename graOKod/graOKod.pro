TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    game.cpp \
    player.cpp \
    deck.cpp \
    card.cpp \
    creaturecard.cpp \
    abilitycard.cpp \
    gamemanager.cpp \
    gameinitializer.cpp \
    gamecreator.cpp \
    stackdeckcreator.cpp \
    cardmanager.cpp \
    normalcreaturecard.cpp \
    actionmanager.cpp \
    action.cpp \
    drawcardaction.cpp \
    actionhandler.cpp \
    deckendexception.cpp \
    gamefinishexception.cpp \
    playcardaction.cpp \
    notenoughtmanaexception.cpp \
    checkcasualitiesaction.cpp \
    checkplayersdeathaction.cpp \
    attackcreatureaction.cpp \
    attackexception.cpp \
    dealdamagetocreatureaction.cpp \
    attackplayeraction.cpp \
    dealdamagetoplayeraction.cpp \
    protectorcreaturecard.cpp \
    untargetablecreaturecard.cpp \
    elusivecreaturecard.cpp \
    stealthcreaturecard.cpp \
    ferocitycreaturecard.cpp \
    healcreatureaction.cpp \
    healplayeraction.cpp \
    aoehealaction.cpp \
    aoedamageaction.cpp \
    readycardaction.cpp \
    destroycardaction.cpp \
    addmanaaction.cpp \
    increaseattackaction.cpp \
    decreaseattackaction.cpp \
    aoeincreaseattackaction.cpp \
    aoedecreaseattackaction.cpp \
    noselectedcardexception.cpp \
    cardfactory.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    game.h \
    player.h \
    deck.h \
    card.h \
    creaturecard.h \
    abilitycard.h \
    gamemanager.h \
    gameinitializer.h \
    gamecreator.h \
    stackdeckcreator.h \
    cardmanager.h \
    normalcreaturecard.h \
    actionmanager.h \
    action.h \
    drawcardaction.h \
    actionhandler.h \
    deckendexception.h \
    gamefinishexception.h \
    playcardaction.h \
    notenoughtmanaexception.h \
    checkcasualitiesaction.h \
    checkplayersdeathaction.h \
    attackcreatureaction.h \
    attackexception.h \
    dealdamagetocreatureaction.h \
    attackplayeraction.h \
    dealdamagetoplayeraction.h \
    protectorcreaturecard.h \
    untargetablecreaturecard.h \
    elusivecreaturecard.h \
    stealthcreaturecard.h \
    ferocitycreaturecard.h \
    healcreatureaction.h \
    healplayeraction.h \
    aoehealaction.h \
    aoedamageaction.h \
    readycardaction.h \
    destroycardaction.h \
    addmanaaction.h \
    increaseattackaction.h \
    decreaseattackaction.h \
    aoeincreaseattackaction.h \
    aoedecreaseattackaction.h \
    noselectedcardexception.h \
    cardfactory.h

CONFIG += c++11

