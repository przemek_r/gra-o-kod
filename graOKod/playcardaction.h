#ifndef PLAYCARDACTION_H
#define PLAYCARDACTION_H
#include "action.h"
class Card;

/**
 * @brief Klasa przechowuje informacje o akcji 'zagranie karty'.
 *
 */
class PlayCardAction : public Action {
private:
    Card* playedCard;
    Card* target;
public:
    /**
     * @brief Konstruktor PlayCardAction
     * @param playedCard_
     * @param target_
     */
    PlayCardAction(Card* playedCard_, Card* target_);
    ~PlayCardAction() {}

    Card* getPlayedCard();
    Card* getTarget();
};

#endif // PLAYCARDACTION_H
