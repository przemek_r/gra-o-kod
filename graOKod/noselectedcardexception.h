#ifndef NOSELECTEDCARDEXCEPTION_H
#define NOSELECTEdCARDEXCEPTION_H
#include "exception"

/**
 * @brief Wyjątek braku wybranej karty.
 *
 */
class NoSelectedCardException : public std::exception
{
public:
    /**
     * @brief Konstruktor NoSelectedCardException
     */
    NoSelectedCardException();

    virtual const char *what() const throw();
};

#endif // NOSELECTEDCARDEXCEPTION_H
