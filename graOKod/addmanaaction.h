#ifndef ADDMANAACTION_H
#define ADDMANAACTION_H
#include "action.h"

class Player;

/**
 * @brief Klasa przechowuje informacje o akcji 'dodaj mane'.
 *  
 */
class AddManaAction : public Action {
private:
    Player* player;
    int value;

public:
    /**
     * @brief Konstruktor AddManaAction
     * @param player_
     * @param value_
     */
    AddManaAction(Player* player_, int value_);

    Player* getPlayer();
    int getValue();
};

#endif // ADDMANAACTION_H
