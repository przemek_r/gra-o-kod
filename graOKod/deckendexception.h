#ifndef DECKENDEXCEPTION_H
#define DECKENDEXCEPTION_H
#include <exception>
#include "player.h"

/**
 * @brief Wyjatek skończenia kart w talii.
 *
 */
class DeckEndException : public std::exception {
private:
    Player* player;
public:
    /**
     * @brief Konstruktor DeckEndException
     * @param player_
     */
    DeckEndException(Player* player_);

    Player* getPlayer();
    virtual const char* what() const throw();
};

#endif // DECKENDEXCEPTION_H
