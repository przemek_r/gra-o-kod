#include "game.h"

Game::Game(int id_, Player* p1_, Player* p2_) {
    id = id_;
    playerOne = p1_;
    playerTwo = p2_;
    activePlayer = playerOne;
    nonActivePlayer = playerTwo;
    currentTurnNumber = 0;
}

Game::~Game() {
    delete playerOne;
    delete playerTwo;
}

Player *Game::getPlayerOne() {
    return playerOne;
}

Player *Game::getPlayerTwo() {
    return playerTwo;
}

Player *Game::getPlayer(int id) {
    if (id == 1) {
        return playerOne;
    }

    return playerTwo;
}

Player *Game::getActivePlayer() {
    return activePlayer;
}

Player *Game::getNonActivePlayer(){
    return nonActivePlayer;
}

Player *Game::getLocalPlayer() {
    return localPlayer;
}

Player *Game::getOtherPlayer(Player *player) {
    if (playerOne == player) {
        return playerTwo;
    }

    return playerOne;
}

int Game::getId(){
    return id;
}

void Game::setId(int _id) {
    id = _id;
}

void Game::setPlayerOne(Player *p1_) {
    playerOne = p1_;
}

int Game::getCurrentTurnNumber() {
    return currentTurnNumber;
}

void Game::setPlayerTwo(Player *p2_)
{
    playerTwo = p2_;
}

void Game::setLocalPlayer(Player *p_) {
    localPlayer = p_;
}

void Game::currentTurnNumberIncrement()
{
    currentTurnNumber++;
}

void Game::changeActivePlayer()
{
    if (activePlayer ==  playerOne) {
        activePlayer = playerTwo;
        nonActivePlayer = playerOne;
    } else {
        activePlayer = playerOne;
        nonActivePlayer = playerTwo;
    }
}

void Game::initActivePlayer() {
    activePlayer = playerOne;
    nonActivePlayer = playerTwo;
}

