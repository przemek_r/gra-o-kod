#ifndef GAMECREATOR_H
#define GAMECREATOR_H


class Game;

enum DeckType{
    DefaultDeck = 0,
    CustomDeck = 1
};


/**
 * @brief Fabryka gry.
 *
 */
class   GameCreator {
private:
    GameCreator() {}
public:

    /**
     * @brief Tworzy grę testową.
     * @return
     */
    static Game* createTestGame();

    /**
     * @brief Tworzy grę w trybie gorących krzeseł.
     * @param deckType
     * @return
     */
    static Game* createHotSeats(DeckType deckType);

    /**
     * @brief Tworzy grę w trybie sieciowym.
     * @param deckType
     * @return
     */
    static Game* createMultiplayer(DeckType deckType);


    /**
     * @brief Torzy grę przygotowaną do dołączenia.
     * @param deckType
     * @return
     */
    static Game* createJoinMultiplayer(DeckType deckType);
};

#endif // GAMECREATOR_H
