#ifndef AOEINCREASEATTACKACTION_H
#define AOEINCREASEATTACKACTION_H
#include "action.h"

/**
 * @brief Klasa przechowuje informacje o zdarzeniu 'obszarowe zwiększenie ataku'.
 *
 */
class AoeIncreaseAttackAction : public Action {
private:
    int value;

public:
    /**
     * @brief Konstruktor AoeIncreaseAttackAction
     * @param value_
     */
    AoeIncreaseAttackAction(int value_);
    AoeIncreaseAttackAction() {}

    int getValue();
};

#endif // AOEINCREASEATTACKACTION_H
