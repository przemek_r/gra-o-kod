#ifndef ACTIONHANDLER_H
#define ACTIONHANDLER_H

class Action;
class DrawCardAction;
class ActionManager;
class Game;

/**
 * @brief Interfejs wykorzystywany we wzorcu projektowym 'łańcuch zobowiązań'.
 *
 */
class ActionHandler {
protected:
    ActionHandler* next;
    ActionManager* actionManager;
    Game* game;

public:
    /**
     * @brief Konstruktor ActionHandler
     * @param actionManager_
     * @param game_
     */
    ActionHandler(ActionManager* actionManager_, Game* game_);

    virtual ~ActionHandler() {}

    /**
     * @brief Wywołuje funkcję obsługującą zdarzenie.
     * @param action_
     */
    virtual void handle(Action* action_)=0;

    /**
     * @brief Dodaje do łańcucha klasę obsługującą zdarzenie.
     * @param nextHandler_
     */
    void addHandler(ActionHandler* nextHandler_);
};

/**
 * @brief Klasa obsługująca pociągnięcie karty.
 *
 */
class DrawCardActionHandler : public ActionHandler {
public:
    /**
     * @brief Kontruktor DrawCardActionHandler
     * @param actionManager_
     * @param game_
     */
    DrawCardActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca zagranie karty.
 *
 */
class PlayCardActionHandler : public ActionHandler {
public:
    /**
     * @brief Kontruktor PlayCardActionHandler
     * @param actionManager_
     * @param game_
     */
    PlayCardActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obłsugująca śmierć kreatur.
 *
 */
class CheckCasualtiesActionHandler : public ActionHandler {
public:
    /**
     * @brief Kontruktor CheckCasualtiesActionHandler
     * @param actionManager_
     * @param game_
     */
    CheckCasualtiesActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};


/**
 * @brief Klasa obsługująca śmierć graczy i koniec rozgrywki
 *
 */
class CheckPlayersDeathActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor CheckPlayersDeathActionHandler
     * @param actionManager_
     * @param game_
     */
    CheckPlayersDeathActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca atak na kreaturę
 *
 */
class AttackCreatureActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor AttackCreatureActionHandler
     * @param actionManager_
     * @param game_
     */
    AttackCreatureActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca zadawanie obrażeń kreaturom
 *
 */
class DealDamageToCreatureActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor DealDamageToCreatureActionHandler
     * @param actionManager_
     * @param game_
     */
    DealDamageToCreatureActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca atak na gracza
 *
 */
class AttackPlayerActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor AttackPlayerActionHandler
     * @param actionManager_
     * @param game_
     */
    AttackPlayerActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca zadawanie obrażeń graczom
 *
 */
class DealDamageToPlayerActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor DealDamageToPlayerActionHandler
     * @param actionManager_
     * @param game_
     */
    DealDamageToPlayerActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca leczenie kreatur
 *
 */
class HealCreatureActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor HealCreatureActionHandler
     * @param actionManager_
     * @param game_
     */
    HealCreatureActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca leczenie gracza
 *
 */
class HealPlayerActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor HealPlayerActionHandler
     * @param actionManager_
     * @param game_
     */
    HealPlayerActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca obszarowe leczenie
 *
 */
class AoeHealActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor AoeHealActionHandler
     * @param actionManager_
     * @param game_
     */
    AoeHealActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca obszarowe obrażenia
 *
 */
class AoeDamageActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor AoeDamageActionHandler
     * @param actionManager_
     * @param game_
     */
    AoeDamageActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca przygotowanie karty
 *
 */
class ReadyCardActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor ReadyCardActionHandler
     * @param actionManager_
     * @param game_
     */
    ReadyCardActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca zniszczenie kreatury
 *
 */
class DestroyCardActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor DestroyCardActionHandler
     * @param actionManager_
     * @param game_
     */
    DestroyCardActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};


/**
 * @brief Klasa obsługująca dodanie many
 *
 */
class AddManaActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor AddManaActionHandler
     * @param actionManager_
     * @param game_
     */
    AddManaActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca zwiekszenie ataku
 *
 */
class IncreaseAttackActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor IncreaseAttackActionHandler
     * @param actionManager_
     * @param game_
     */
    IncreaseAttackActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca zmniejszenie ataku
 *
 */
class DecreaseAttackActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor DecreaseAttackActionHandler
     * @param actionManager_
     * @param game_
     */
    DecreaseAttackActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca obszarowe zwiększenie ataku
 *
 */
class AoeIncreaseAttackActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor AoeIncreaseAttackActionHandler
     * @param actionManager_
     * @param game_
     */
    AoeIncreaseAttackActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

/**
 * @brief Klasa obsługująca obszarowe zmniejszenie ataku
 *
 */
class AoeDecreaseAttackActionHandler : public ActionHandler {
public:
    /**
     * @brief Konstruktor AoeDecreaseAttackActionHandler
     * @param actionManager_
     * @param game_
     */
    AoeDecreaseAttackActionHandler(ActionManager* actionManager_, Game* game_) : ActionHandler(actionManager_, game_) {}

    void handle(Action *action_);
};

#endif // ACTIONHANDLER_H
