#ifndef DECK_H
#define DECK_H
#include <vector>

class Card;
typedef std::vector<Card*> CardVector;


/**
 * @brief Zawier zbiór kart.
 *
 */
class Deck {
private:
    CardVector cards;
public:


    /**
     * @brief Konstruktor Deck
     */
    Deck();

    /**
     * @brief Konstruktor Deck
     * @param list
     */
    Deck(CardVector list);
    ~Deck();

    CardVector& getCards();

    /**
     * @brief Dodaje karte do talii.
     * @param card
     */
    void addCard(Card* card);

};

#endif // DECK_H
