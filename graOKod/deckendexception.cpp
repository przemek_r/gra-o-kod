#include "deckendexception.h"
#include "string"

DeckEndException::DeckEndException(Player* player_)
{
    player = player_;
}

Player *DeckEndException::getPlayer()
{
    return player;
}

const char* DeckEndException::what() const throw()
{
    return "Koniec talii gracza";
}

