#ifndef ABILITYCARD_H
#define ABILITYCARD_H
#include "card.h"

/**
 * @brief Klasa przedstawiająca kartę umiejętności.
 *
 */
class AbilityCard : public Card {
public:
    AbilityCard() {}
    ~AbilityCard() {}
};

class AbilityCard1 : public AbilityCard {
public:
    /**
     * @brief Konstruktor AbilityCard1
     * @param id_
     */
    AbilityCard1(int id_);
    ~AbilityCard1() {}

    void putAction(ActionManager* actionManager, Card* target);
};

class AbilityCard2 : public AbilityCard {
public:
    /**
     * @brief Konstruktor AbilityCard2
     * @param id_
     */
    AbilityCard2(int id_);
    ~AbilityCard2() {}

    void putAction(ActionManager* actionManager, Card* target);
};

class AbilityCard3 : public AbilityCard {
public:
    /**
     * @brief Konstruktor AbilityCard3
     * @param id_
     */
    AbilityCard3(int id_);
    ~AbilityCard3() {}

    void putAction(ActionManager* actionManager, Card* target);
};

class AbilityCard10 : public AbilityCard {
public:
    /**
     * @brief Konstruktor AbilityCard10
     * @param id_
     */
    AbilityCard10(int id_);
    ~AbilityCard10() {}

    void putAction(ActionManager* actionManager, Card* target);
};

class AbilityCard16 : public AbilityCard {
public:
    /**
     * @brief Konstruktor AbilityCard16
     * @param id_
     */
    AbilityCard16(int id_);
    ~AbilityCard16() {}

    void putAction(ActionManager* actionManager, Card* target);
};

class AbilityCard18 : public AbilityCard {
public:
    /**
     * @brief Kontruktor AbilityCard18
     * @param id_
     */
    AbilityCard18(int id_);
    ~AbilityCard18() {}

    void putAction(ActionManager* actionManager, Card* target);
};

class AbilityCard21 : public AbilityCard {
public:
    /**
     * @brief Konstruktor AbilityCard21
     * @param id_
     */
    AbilityCard21(int id_);
    ~AbilityCard21() {}

    void putAction(ActionManager* actionManager, Card* target);
};


#endif // ABILITYCARD_H
