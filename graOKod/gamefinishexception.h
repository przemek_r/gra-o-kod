#ifndef GAMEFINISHEXCEPTION_H
#define GAMEFINISHEXCEPTION_H
#include <exception>
#include <string>

/**
 * @brief Wyjątek zakończenia gry.
 *
 */
class GameFinishException : public std::exception {
private:
    int winnerId;
    std::string name;
public:
    /**
     * @brief Konstruktor GameFinishException
     * @param id
     * @param name_
     */
    GameFinishException(int id, std::string name_);

    virtual const char* what() const throw();
    std::string winner();
    std::string winnerName();
};

#endif // GAMEFINISHEXCEPTION_H
