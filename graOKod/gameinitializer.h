#ifndef GAMEINITIALIZER_H
#define GAMEINITIALIZER_H

class Game;

/**
 * @brief Klasa obsługująca inicjalizacje gry.
 *
 */
class GameInitializer {
private:
    GameInitializer() {}
public:
    /**
     * @brief Inicjalizuje gre w trybie gorące krzesła.
     * @return
     */
    static Game* initGame();

    /**
     * @brief Inicjalizuje stworzenie gry multiplayer.
     * @return
     */
    static Game* initMultiplayerGame();

    /**
     * @brief Inicjalizuje grę przy dołączeniu do gry.
     * @return
     */
    static Game* initJoinMultiplayerGame();
};

#endif // GAMEINITIALIZER_H
