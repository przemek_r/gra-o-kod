#include "stackdeckcreator.h"
#include "deck.h"
#include "card.h"
#include "creaturecard.h"
#include "normalcreaturecard.h"
#include "protectorcreaturecard.h"
#include "untargetablecreaturecard.h"
#include "elusivecreaturecard.h"
#include "stealthcreaturecard.h"
#include "ferocitycreaturecard.h"
#include "abilitycard.h"
#include <algorithm>
#include <chrono>
#include "cardfactory.h"
#include <random>
#include <fstream>
#include <iostream>

Deck* StackDeckCreator::generateTestDeck(int cardCounter) {
    CardVector stack;
    for (int i = 0; i < 2; ++i) {
        for(int j = 1; j <= 21; ++j) {
            stack.push_back(CardFactory::createCardBySetId(j, cardCounter++));
        }
    }

    //tasowanie kart
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::shuffle(stack.begin(),stack.end(),std::default_random_engine(seed));

    return new Deck(stack);
}

Deck *StackDeckCreator::loadDeckFromFile(int cardCounter) {
    CardVector stack;
    std::ifstream ifs("deck.txt");
    int cardId;

    for (int i = 1; i <= 30; ++i) {
        ifs >> cardId;
        stack.push_back(CardFactory::createCardBySetId(cardId, cardCounter++));
    }

    ifs.close();
    //tasowanie kart
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::shuffle(stack.begin(),stack.end(),std::default_random_engine(seed));

    return new Deck(stack);
}

Deck *StackDeckCreator::createFromJson(rapidjson::Value &deck) {
    Deck* result = new Deck;
    int id;
    int setId;
    for (int i = 0; i < deck.Size(); ++i) {
        id = deck[i]["id"].GetInt();
        setId = deck[i]["cardTypeId"].GetInt();
        result->addCard(CardFactory::createCardBySetId(setId, id));
    }
    return result;
}

