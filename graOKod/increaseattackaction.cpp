#include "increaseattackaction.h"



IncreaseAttackAction::IncreaseAttackAction(Card *target_, int value_) {
    target = target_;
    value = value_;
}

Card *IncreaseAttackAction::getTarget() {
    return target;
}

int IncreaseAttackAction::getValue() {
    return value;
}
