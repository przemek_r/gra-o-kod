#include "actionmanager.h"
#include "actionhandler.h"
#include "action.h"
#include <iterator>

ActionManager::ActionManager(Game *game_)
{
    game = game_;
    actionDeque = std::deque<Action*>();
    actionHandlerVector = std::vector<ActionHandler*>();

    // stworzenie łańcucha zobowiązań
    actionHandlerVector.push_back(new DrawCardActionHandler(this,game));
    actionHandlerVector.push_back(new PlayCardActionHandler(this,game));
    actionHandlerVector.push_back(new CheckCasualtiesActionHandler(this,game));
    actionHandlerVector.push_back(new CheckPlayersDeathActionHandler(this,game));
    actionHandlerVector.push_back(new AttackCreatureActionHandler(this,game));
    actionHandlerVector.push_back(new DealDamageToCreatureActionHandler(this,game));
    actionHandlerVector.push_back(new AttackPlayerActionHandler(this,game));
    actionHandlerVector.push_back(new DealDamageToPlayerActionHandler(this,game));
    actionHandlerVector.push_back(new HealCreatureActionHandler(this,game));
    actionHandlerVector.push_back(new HealPlayerActionHandler(this,game));
    actionHandlerVector.push_back(new AoeHealActionHandler(this,game));
    actionHandlerVector.push_back(new AoeDamageActionHandler(this,game));
    actionHandlerVector.push_back(new ReadyCardActionHandler(this,game));
    actionHandlerVector.push_back(new DestroyCardActionHandler(this,game));
    actionHandlerVector.push_back(new AddManaActionHandler(this,game));
    actionHandlerVector.push_back(new IncreaseAttackActionHandler(this,game));
    actionHandlerVector.push_back(new DecreaseAttackActionHandler(this,game));
    actionHandlerVector.push_back(new AoeIncreaseAttackActionHandler(this,game));
    actionHandlerVector.push_back(new AoeDecreaseAttackActionHandler(this,game));
    for( std::vector<ActionHandler*>::iterator it = actionHandlerVector.begin(); it != actionHandlerVector.end()-1; ++it){
        (*(it+1))->addHandler(*it);
    }
}

ActionManager::~ActionManager() {
    for (
        std::vector<ActionHandler*>::iterator it = actionHandlerVector.begin();
        it != actionHandlerVector.end();
        ++it
    ) {
        delete (*it);
    }
}

void ActionManager::addAction(Action *action_)
{
    actionDeque.push_back(action_);
}

void ActionManager::resolveActions()
{
    //łańcuch zobowiązań jazda
    while(!actionDeque.empty()){
        actionHandlerVector.back()->handle(actionDeque.front());
        actionDeque.pop_front();
    }
}

void ActionManager::clearActionDeque() {
    for (std::deque<Action*>::iterator it = actionDeque.begin(); it != actionDeque.end(); ++it) {
        delete (*it);
    }
    actionDeque.clear();
}

Game *ActionManager::getGame() {
    return game;
}



