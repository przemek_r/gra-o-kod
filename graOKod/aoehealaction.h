#ifndef AOEHEALACTION_H
#define AOEHEALACTION_H
#include "action.h"

/**
 * @brief Klasa przechowuje informacje o zdarzeniu 'leczenie obszarowe'.
 *
 */
class AoeHealAction : public Action {
private:
    int heal;

public:
    /**
     * @brief Konstruktor AoeHealAction
     * @param heal_
     */
    AoeHealAction(int heal_);
    AoeHealAction() {}

    int getHeal();
};

#endif // AOEHEALACTION_H
