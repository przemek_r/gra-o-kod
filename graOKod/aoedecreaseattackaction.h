#ifndef AOEDECREASEATTACKACTION_H
#define AOEDECREASEATTACKACTION_H
#include "action.h"

/**
 * @brief Klasa przechowuje informacje o zdarzeniu 'obszarowe zmniejszenie ataku'.
 *
 */
class AoeDecreaseAttackAction : public Action {
private:
    int value;
public:
    /**
     * @brief Konstruktor AoeDecreaseAttackAction
     * @param value_
     */
    AoeDecreaseAttackAction(int value_);
    ~AoeDecreaseAttackAction() {}

    int getValue();
};

#endif // AOEDECREASEATTACKACTION_H
