#include <iostream>
#include "gamemanager.h"
#include "game.h"
#include "player.h"
#include "deck.h"
#include "card.h"
#include <algorithm>
#include "notenoughtmanaexception.h"
#include "gamefinishexception.h"
#include "attackexception.h"
#include <stdlib.h>

using namespace std;

void printCards(Card* card)
{
    cout << card->getCost() << "/" << card->getAttack() << "/" << card->getHealth() << ", ";
    if(card->isExhausted()) {
        cout << "EXHAUSTED, ";
    }
    cout << card->getDescription();

    cout << endl;
}


void printGameTable(Game* game){

    system("clear");

    cout << "Aktywny gracz: " << game->getActivePlayer()->getId() << ". HP: " << game->getActivePlayer()->getHealth() << endl;

    cout << "HP przeciwnika: " << game->getNonActivePlayer()->getHealth() << endl;

    cout << "Stół przeciwnika:" << endl;
    for_each(game->getNonActivePlayer()->getTableDeck()->getCards().begin(),
             game->getNonActivePlayer()->getTableDeck()->getCards().end(), printCards);

    cout << "Stół gracza: " << endl;
    for_each(game->getActivePlayer()->getTableDeck()->getCards().begin(),
             game->getActivePlayer()->getTableDeck()->getCards().end(), printCards);

    cout << "Reka gracza:" << endl;
    std::for_each(game->getActivePlayer()->getHandDeck()->getCards().begin(),
                  game->getActivePlayer()->getHandDeck()->getCards().end(), printCards);

    cout << "Mana: " << game->getActivePlayer()->getMana() << endl;
}


int main() {
    int choosenCard;
    int attackerCard;
    int defenderCard;
    int targetCard;
    int action;
    bool gameFinished = false;

    GameManager* gameManager = new GameManager();
    gameManager->startGame();
    gameManager->startTurn();

    while(!gameFinished){
            printGameTable(gameManager->getGame());

            cout << "Wybierz akcje (1 - zagraj karte, 2 - zaatakuj stwora, 3 - zaatakuj gracza, 4 - zakończ ture): ";
            cin >> action;
            switch (action) {
            case 1:
                cout << "Wybierz kartę: ";
                cin >> choosenCard;
                choosenCard--;
                cout << "Wybierz cel (0 - brak celu): ";
                cin >> targetCard;
                targetCard--;
                try {
                    gameManager->activePlayerPlayCard(gameManager->getGame()->getActivePlayer()->getHandDeck()->getCards()[choosenCard],
                                                      (targetCard == -1)? nullptr : gameManager->getGame()->getNonActivePlayer()->getTableDeck()->getCards()[targetCard] );
                } catch (NotEnoughtManaException& ex) {
                    cout << ex.what() << endl;
                    cout << "Wcisnij przycisk, aby kontynuować...";
                    cin.ignore();
                    cin.get();
                } catch (GameFinishException& ex){
                    gameFinished = true;
                    cout << ex.what() << endl;
                    cout << ex.winner() << endl;
                    cout << "Wcisnij przycisk, aby kontynuować...";
                    cin.ignore();
                    cin.get();
                } catch (AttackException& ex){
                    cout << ex.what() << endl;
                    cout << "Wcisnij przycisk, aby kontynuować...";
                    cin.ignore();
                    cin.get();
                }
                break;
            case 2:
                cout << "Wybierz kartę atakującą: ";
                cin >> attackerCard;
                attackerCard--;
                cout << "Wybierz cel: ";
                cin >> defenderCard;
                defenderCard--;
                try {
                    gameManager->attackCreature(gameManager->getGame()->getActivePlayer()->getTableDeck()->getCards()[attackerCard],
                                                gameManager->getGame()->getNonActivePlayer()->getTableDeck()->getCards()[defenderCard]);
                } catch (GameFinishException& ex){
                    gameFinished = true;
                    cout << ex.what() << endl;
                    cout << ex.winner() << endl;
                    cout << "Wcisnij przycisk, aby kontynuować...";
                    cin.ignore();
                    cin.get();
                } catch (exception& ex) {
                    cout << ex.what() << endl;
                    cout << "Wcisnij przycisk, aby kontynuować...";
                    cin.ignore();
                    cin.get();
                }
                break;
            case 3:
                cout << "Wybierz kartę atakującą: ";
                cin >> attackerCard;
                attackerCard--;
                try {
                    gameManager->attackPlayer(gameManager->getGame()->getActivePlayer()->getTableDeck()->getCards()[attackerCard]);
                } catch (GameFinishException& ex){
                    gameFinished = true;
                    cout << ex.what() << endl;
                    cout << ex.winner() << endl;
                    cout << "Wcisnij przycisk, aby kontynuować...";
                    cin.ignore();
                    cin.get();
                } catch (exception& ex) {
                    cout << ex.what() << endl;
                    cout << "Wcisnij przycisk, aby kontynuować...";
                    cin.ignore();
                    cin.get();
                }
                break;
            case 4:
                gameManager->endTurn();
                gameManager->startTurn();
                gameManager->startTurnActions();
                break;
            default:
                break;
            }
    }
    return 0;
}

