#ifndef CHECKPLAYERSDEATHACTION_H
#define CHECKPLAYERSDEATHACTION_H
#include "action.h"

/**
 * @brief Klasa przechowuje informacje o akcji 'sprawdzenie śmierci graczy'.
 *
 */
class CheckPlayersDeathAction : public Action{
public:
    /**
     * @brief Konstruktor CheckPlayersDeathAction
     */
    CheckPlayersDeathAction();
    ~CheckPlayersDeathAction() {}
};

#endif // CHECKPLAYERSDEATHACTION_H
