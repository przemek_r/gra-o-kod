#include "decreaseattackaction.h"


DecreaseAttackAction::DecreaseAttackAction(Card *target_, int value_) {
    target = target_;
    value = value_;
}

Card *DecreaseAttackAction::getTarget() {
    return target;
}

int DecreaseAttackAction::getValue() {
    return value;
}
