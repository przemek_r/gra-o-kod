#include "attackexception.h"


const char *ElusivnessException::what() const throw()
{
    return "Cel jest nieuchwytny!";
}

const char *UntargetableException::what() const throw()
{
    return "Cel jest nienamierzalny!";
}

const char *ExhaustedException::what() const throw()
{
    return "Karta jest wyczerpana!";
}

const char *ProtectorException::what() const throw()
{
    return "Na stole przeciwnika znajdują się obrońcy!";
}
