#include "healplayeraction.h"



HealPlayerAction::HealPlayerAction(int value_, Player *player_) {
    heal = value_;
    player = player_;
}

int HealPlayerAction::getHeal() {
    return heal;
}

Player *HealPlayerAction::getPlayer() {
    return player;
}
