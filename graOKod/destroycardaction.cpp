#include "destroycardaction.h"

DestroyCardAction::DestroyCardAction(Card *target_) {
    target = target_;
}

Card *DestroyCardAction::getTarget() {
    return target;
}

