#ifndef DEALDAMAGETOCREATUREACTION_H
#define DEALDAMAGETOCREATUREACTION_H
#include "action.h"

class Card;

/**
 * @brief Klasa przechowuje informacje o akcji 'zadanie obrażeń kreaturze'.
 *
 */
class DealDamageToCreatureAction : public Action {
private:
    Card* card;
    int damage;

public:
    /**
     * @brief Konstruktor DealDamageToCreatureAction
     * @param damage_
     * @param card_
     */
    DealDamageToCreatureAction(int damage_, Card* card_);
    DealDamageToCreatureAction() {}

    int getDamage();
    Card* getCard();
};

#endif // DEALDAMAGETOCREATUREACTION_H
