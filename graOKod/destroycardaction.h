#ifndef DESTROYCARDACTION_H
#define DESTROYCARDACTION_H
#include "action.h"

class Card;

/**
 * @brief Klasa przechowuje informacje o akcji 'znieszczenie karty'.
 *
 */
class DestroyCardAction : public Action {
private:
    Card* target;

public:
    /**
     * @brief Konstruktor DestroyCardAction
     * @param target_
     */
    DestroyCardAction(Card* target_);
    ~DestroyCardAction() {}

    Card* getTarget();
};

#endif // DESTROYCARDACTION_H
