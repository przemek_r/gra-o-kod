#ifndef PLAYER_H
#define PLAYER_H
#include "deck.h"
#include <string>

class Card;

/**
 * @brief Klasa przedstawia pojedyńczego gracza oraz zawiera talie którymi dysponuje.
 *
 */
class Player {
private:
    int id;
    Deck* handDeck;
    Deck* tableDeck;
    Deck* stackDeck;
    Deck* removedDeck;
    int mana;
    int health;
    std::string name;
public:
    /**
     * @brief Konstruktor Player
     */
    Player();

    /**
     * @brief Konstruktor Player
     * @param deck
     * @param id_
     */
    Player(Deck* deck, int id_);
    ~Player();

    Deck* getHandDeck();
    Deck* getTableDeck();
    Deck* getStackDeck();
    Deck* getRemovedDeck();

    void setId(int id_);
    void setMana(int mana_);
    void setName(std::string name_);

    /**
     * @brief Dodaje manę.
     * @param mana_
     */
    void addMana(int mana_);

    /**
     * @brief Odejmuje manę.
     * @param mana_
     */
    void subtractMana(int mana_);

    /**
     * @brief Zwiększa życie.
     * @param val
     */
    void increaseHealth(int val);

    /**
     * @brief Zmniejsza życie.
     * @param val
     */
    void decreaseHealth(int val);
    int getMana();
    int getHealth();
    int getId();
    std::string getName();
};

#endif // PLAYER_H
