#ifndef DECREASEATTACKACTION_H
#define DECREASEATTACKACTION_H
#include "action.h"

class Card;

/**
 * @brief Klasa przechowuje informacje o akcji 'obniżenie ataku'.
 *
 */
class DecreaseAttackAction : public Action {
private:
    Card* target;
    int value;
public:
    /**
     * @brief Konstruktor DecreaseAttackAction
     * @param target_
     * @param value_
     */
    DecreaseAttackAction(Card* target_, int value_);
    DecreaseAttackAction() {}

    Card* getTarget();
    int getValue();
};

#endif // DECREASEATTACKACTION_H
