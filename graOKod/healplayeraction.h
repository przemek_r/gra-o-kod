#ifndef HEALPLAYERACTION_H
#define HEALPLAYERACTION_H
#include "action.h"

class Player;

/**
 * @brief Klasa przechowuje informacje o akcji 'uleczenie gracza'.
 *
 */
class HealPlayerAction : public Action {

private:
    int heal;
    Player* player;
public:
    /**
     * @brief Konstruktor HealPlayerAction
     * @param value_
     * @param player_
     */
    HealPlayerAction(int value_, Player* player_);
    HealPlayerAction() {}

    int getHeal();
    Player* getPlayer();
};

#endif // HEALPLAYERACTION_H
