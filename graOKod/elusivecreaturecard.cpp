#include "elusivecreaturecard.h"
#include "actionmanager.h"
#include "aoeincreaseattackaction.h"


bool ElusiveCreatureCard::isElusive() {
    return true;
}

ElusiveCreatureCard6::ElusiveCreatureCard6(int id_) {
    id = id_;
    setId = 6;
    name = "Pyłek";
    attack = 2;
    health = 1;
    cost = 1;
    exhausted = false;
    desctription = "NIEUCHWYTNYY";
}

ElusiveCreatureCard20::ElusiveCreatureCard20(int id_) {
    id = id_;
    setId = 20;
    name = "Dekorator";
    attack = 2;
    health = 2;
    cost = 3;
    exhausted = false;
    desctription = "NIEUCHWYTNYY\n Przy zagraniu zwiększa atak kreatur gracza o 1";
}

void ElusiveCreatureCard20::putAction(ActionManager *actionManager, Card *target) {
    actionManager->addAction(new AoeIncreaseAttackAction(1));
}
