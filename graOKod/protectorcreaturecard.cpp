#include "protectorcreaturecard.h"
#include "actionmanager.h"
#include "healcreatureaction.h"
#include "aoedamageaction.h"
#include "checkcasualitiesaction.h"
#include "checkplayersdeathaction.h"
#include "cardmanager.h"
#include "game.h"

bool ProtectorCreatureCard::isProtector() {
    return true;
}

ProtectorCreatureCard4::ProtectorCreatureCard4(int id_) {
    id = id_;
    setId = 4;
    name = "Antivirus";
    attack = 1;
    health = 5;
    cost = 5;
    exhausted = false;
    desctription = "PROWOKATOR\n Na początku tury leczy się o 1";
}

void ProtectorCreatureCard4::atTurnStartAction(ActionManager *actionManager) {
    actionManager->addAction(new HealCreatureAction(1, this));
}

ProtectorCreatureCard13::ProtectorCreatureCard13(int id_) {
    id = id_;
    setId = 13;
    name = "Kompilator";
    attack = 2;
    health = 3;
    cost = 3;
    exhausted = false;
    desctription = "PROWOKATOR";
}

ProtectorCreatureCard17::ProtectorCreatureCard17(int id_) {
    id = id_;
    setId = 17;
    name = "Semafor";
    attack = 1;
    health = 2;
    cost = 1;
    exhausted = false;
    desctription = "PROWOKATOR";
}

ProtectorCreatureCard19::ProtectorCreatureCard19(int id_) {
    id = id_;
    setId = 19;
    name = "Boski obiekt";
    attack = 5;
    health = 6;
    cost = 8;
    exhausted = false;
    desctription = "PROWOKATOR\n Po zniszczeniu zadaje przeciwnikowi i wszystkim jego kreaturom 2 obrażenia";
}

void ProtectorCreatureCard19::destroyedAction(ActionManager *actionManager) {
    Player* targetPlayer = actionManager->getGame()->getOtherPlayer(cardManager::getCardOwner(this, actionManager->getGame()));
    actionManager->addAction(new AoeDamageAction(targetPlayer, 2));
    actionManager->addAction(new CheckCasualtiesAction);
    actionManager->addAction(new CheckPlayersDeathAction);
}
