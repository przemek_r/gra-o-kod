#ifndef NOTENOUGHTMANAEXCEPTION_H
#define NOTENOUGHTMANAEXCEPTION_H
#include <exception>

/**
 * @brief Wyjatek braku many.
 *
 */
class NotEnoughtManaException : public std::exception {
public:
    /**
     * @brief Konstruktor NotEnoughtManaException
     */
    NotEnoughtManaException();

    virtual const char* what() const throw();
};

#endif // NOTENOUGHTMANAEXCEPTION_H
