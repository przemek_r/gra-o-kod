#ifndef INCREASEATTACKACTION_H
#define INCREASEATTACKACTION_H
#include "action.h"

class Card;

/**
 * @brief Klasa przechowuje informacje o akcji 'zwiększenie ataku'.
 *
 */
class IncreaseAttackAction : public Action {
private:
    Card* target;
    int value;
public:
    /**
     * @brief Konstruktor IncreaseAttackAction
     * @param target_
     * @param value_
     */
    IncreaseAttackAction(Card* target_, int value_);
    IncreaseAttackAction() {}

    Card* getTarget();
    int getValue();
};

#endif // INCREASEATTACKACTION_H
