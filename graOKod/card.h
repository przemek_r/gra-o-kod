#ifndef CARD_H
#define CARD_H

#include "string"

class ActionManager;
class Game;

/**
 * @brief Interfejs karty.
 *
 */
class Card {
protected:
    std::string name;
    int setId;
    int id;
    int cost;
    int attack;
    int health;
    bool exhausted;
    std::string desctription;
public:

    virtual ~Card();

    /**
     * @brief Akcja przy wyłożeniu karty.
     * @param actionManager
     * @param target
     */
    virtual void putAction(ActionManager* actionManager, Card* target) {}

    /**
     * @brief Akcja przy ataka.
     * @param actionManager
     */
    virtual void attackAction(ActionManager* actionManager) {}

    /**
     * @brief Akcja przy zniszczeniu karty.
     * @param actionManager
     */
    virtual void destroyedAction(ActionManager* actionManager) {}

    /**
     * @brief Akcja na początku tury.
     * @param actionManager
     */
    virtual void atTurnStartAction(ActionManager* actionManager) {}

    /**
     * @brief Akcja na końcu tury
     * @param actionManager
     */
    virtual void atTurnEndAction(ActionManager* actionManager) {}

    int getId();
    int getCost();
    bool isExhausted();
    std::string getName();
    int getSetId();
    std::string getDescription();
    int getAttack();
    int getHealth();

    /**
     * @brief Ustawia wyczerpanie.
     * @param exhausted_
     */
    void setExhausted(bool exhausted_);

    /**
     * @brief Zwieksza życie.
     * @param val
     */
    void increaseHealth(int val);

    /**
     * @brief Obniża życie.
     * @param val
     */
    void decreaseHealth(int val);

    /**
     * @brief Zwiększa atak.
     * @param val
     */
    void increaseAttack(int val);

    /**
     * @brief Obniża atak.
     * @param val
     */
    void decreaseAttack(int val);

    /**
     * @brief Sprawdza czy karta jest prowokatorem.
     * @return
     */
    virtual bool isProtector();

    /**
     * @brief Sprawdza czy karta jest nietykalna.
     * @return
     */
    virtual bool isElusive();

    /**
     * @brief Sprawdza czy karta posiada zdolność 'ukrycie'.
     * @return
     */
    virtual bool isStealth();

    /**
     * @brief Sprawdza czy karta jest nienamieżalna.
     * @return
     */
    virtual bool isUntargetable();

    /**
     * @brief Sprawdza czy karta posiada znolność 'szarża'.
     * @return
     */
    virtual bool isFerocity();
};

#endif // CARD_H
