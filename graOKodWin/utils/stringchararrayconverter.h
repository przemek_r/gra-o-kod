#ifndef STRINGCHARARRAYCONVERTER_H
#define STRINGCHARARRAYCONVERTER_H

#include <string>

class StringCharArrayConverter
{
private:
    StringCharArrayConverter() {}
public:
    static char* stringToCharArray(std::string value);
    static const char* stringToConstCharArray(std::string str);
};

#endif // STRINGCHARARRAYCONVERTER_H
