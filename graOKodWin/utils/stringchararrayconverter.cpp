#include "stringchararrayconverter.h"

char* StringCharArrayConverter::stringToCharArray(std::string str)
{
    char *cstr = new char[str.size() + 1];
    std::copy(str.begin(), str.end(), cstr);
    cstr[str.size()] = '\0';

    return cstr;
}

const char* StringCharArrayConverter::stringToConstCharArray(std::string str)
{
    const char *cstr = str.c_str();

    return cstr;
}
