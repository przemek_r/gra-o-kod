#ifndef CARDDATAPARSER_H
#define CARDDATAPARSER_H
#include "string"

class Card;
class Deck;

enum ActionType{
    playCardAction = 0,
    attackCreatureAction = 1,
    attackPlayerAction = 2,
    endTurnAction = 3,
    surrenderAction = 4
};

/**
 * @brief Parser JSON.
 *
 */
class DataParser {
private:
    DataParser() {}
public:
    /**
     * @brief Parser karty.
     * @param card
     * @return
     */
    static std::string cardToJson(Card* card);

    /**
     * @brief Parser talii.
     * @param deck
     * @return
     */
    static std::string deckToJson(Deck* deck);

    /**
     * @brief Parser tworzenia gry.
     * @param nick
     * @param deck
     * @return
     */
    static std::string createGameJsonData(std::string nick, Deck* deck);

    /**
     * @brief Parser dołączania do gry.
     * @param nick
     * @param gameId
     * @param deck
     * @return
     */
    static std::string joinGameJsonData(std::string nick, int gameId, Deck* deck);

    /**
     * @brief Parser wykonywania akcji.
     * @param playerId
     * @param actionType
     * @param card
     * @param target
     * @return
     */
    static std::string makeActionsJsonData(int playerId, ActionType actionType, Card* card, Card* target);
};

#endif // CARDDATAPARSER_H
