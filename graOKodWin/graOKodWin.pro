#-------------------------------------------------
#
# Project created by QtCreator 2015-09-06T05:33:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = graOKodWin
TEMPLATE = app

LIBS += -lcurl


SOURCES += main.cpp\
        mainwindow.cpp \
    cardwidget.cpp \
    ../graOKod/abilitycard.cpp \
    ../graOKod/action.cpp \
    ../graOKod/actionhandler.cpp \
    ../graOKod/actionmanager.cpp \
    ../graOKod/addmanaaction.cpp \
    ../graOKod/aoedamageaction.cpp \
    ../graOKod/aoedecreaseattackaction.cpp \
    ../graOKod/aoehealaction.cpp \
    ../graOKod/aoeincreaseattackaction.cpp \
    ../graOKod/attackcreatureaction.cpp \
    ../graOKod/attackexception.cpp \
    ../graOKod/attackplayeraction.cpp \
    ../graOKod/card.cpp \
    ../graOKod/cardmanager.cpp \
    ../graOKod/checkcasualitiesaction.cpp \
    ../graOKod/checkplayersdeathaction.cpp \
    ../graOKod/creaturecard.cpp \
    ../graOKod/dealdamagetocreatureaction.cpp \
    ../graOKod/dealdamagetoplayeraction.cpp \
    ../graOKod/deck.cpp \
    ../graOKod/deckendexception.cpp \
    ../graOKod/decreaseattackaction.cpp \
    ../graOKod/destroycardaction.cpp \
    ../graOKod/drawcardaction.cpp \
    ../graOKod/elusivecreaturecard.cpp \
    ../graOKod/ferocitycreaturecard.cpp \
    ../graOKod/game.cpp \
    ../graOKod/gamecreator.cpp \
    ../graOKod/gamefinishexception.cpp \
    ../graOKod/gameinitializer.cpp \
    ../graOKod/gamemanager.cpp \
    ../graOKod/healcreatureaction.cpp \
    ../graOKod/healplayeraction.cpp \
    ../graOKod/increaseattackaction.cpp \
    ../graOKod/noselectedcardexception.cpp \
    ../graOKod/normalcreaturecard.cpp \
    ../graOKod/notenoughtmanaexception.cpp \
    ../graOKod/playcardaction.cpp \
    ../graOKod/player.cpp \
    ../graOKod/protectorcreaturecard.cpp \
    ../graOKod/readycardaction.cpp \
    ../graOKod/stackdeckcreator.cpp \
    ../graOKod/stealthcreaturecard.cpp \
    ../graOKod/untargetablecreaturecard.cpp \
    renderengine.cpp \
    selectedcardmanager.cpp \
    ../graOKod/cardfactory.cpp \
    dataparser.cpp \
    connection/servercontroller.cpp \
    connection/curlexecutor.cpp \
    exception/postrequestexception.cpp \
    json/jsondecoder.cpp \
    json/creategamejsonresponse.cpp \
    utils/stringchararrayconverter.cpp \
    json/joingamejsonresponse.cpp \
    json/joinedopponentjsonresponse.cpp \
    json/opponentactionjsonresponse.cpp \
    deckbuilder.cpp \
    deckbuilderexception.cpp \
    waitactionthread.cpp \
    controller/playermovecontroller.cpp

HEADERS  += mainwindow.h \
    cardwidget.h \
    ../graOKod/abilitycard.h \
    ../graOKod/action.h \
    ../graOKod/actionhandler.h \
    ../graOKod/actionmanager.h \
    ../graOKod/addmanaaction.h \
    ../graOKod/aoedamageaction.h \
    ../graOKod/aoedecreaseattackaction.h \
    ../graOKod/aoehealaction.h \
    ../graOKod/aoeincreaseattackaction.h \
    ../graOKod/attackcreatureaction.h \
    ../graOKod/attackexception.h \
    ../graOKod/attackplayeraction.h \
    ../graOKod/card.h \
    ../graOKod/cardmanager.h \
    ../graOKod/checkcasualitiesaction.h \
    ../graOKod/checkplayersdeathaction.h \
    ../graOKod/creaturecard.h \
    ../graOKod/dealdamagetocreatureaction.h \
    ../graOKod/dealdamagetoplayeraction.h \
    ../graOKod/deck.h \
    ../graOKod/deckendexception.h \
    ../graOKod/decreaseattackaction.h \
    ../graOKod/destroycardaction.h \
    ../graOKod/drawcardaction.h \
    ../graOKod/elusivecreaturecard.h \
    ../graOKod/ferocitycreaturecard.h \
    ../graOKod/game.h \
    ../graOKod/gamecreator.h \
    ../graOKod/gamefinishexception.h \
    ../graOKod/gameinitializer.h \
    ../graOKod/gamemanager.h \
    ../graOKod/healcreatureaction.h \
    ../graOKod/healplayeraction.h \
    ../graOKod/increaseattackaction.h \
    ../graOKod/noselectedcardexception.h \
    ../graOKod/normalcreaturecard.h \
    ../graOKod/notenoughtmanaexception.h \
    ../graOKod/playcardaction.h \
    ../graOKod/player.h \
    ../graOKod/protectorcreaturecard.h \
    ../graOKod/readycardaction.h \
    ../graOKod/stackdeckcreator.h \
    ../graOKod/stealthcreaturecard.h \
    ../graOKod/untargetablecreaturecard.h \
    renderengine.h \
    selectedcardmanager.h \
    ../graOKod/cardfactory.h \
    dataparser.h \
    connection/servercontroller.h \
    connection/curlexecutor.h \
    exception/postrequestexception.h \
    ../rapidjson/document.h \
    json/jsondecoder.h \
    json/creategamejsonresponse.h \
    utils/stringchararrayconverter.h \
    json/joingamejsonresponse.h \
    json/joinedopponentjsonresponse.h \
    json/opponentactionjsonresponse.h \
    deckbuilder.h \
    deckbuilderexception.h \
    waitactionthread.h \
    controller/playermovecontroller.h

FORMS    += mainwindow.ui

DISTFILES +=

CONFIG += c++11

