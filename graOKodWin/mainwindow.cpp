#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QtWidgets"
#include "QPushButton"
#include "QFrame"
#include "QBoxLayout"
#include "cardwidget.h"
#include "QMessageBox"
#include "../graOKod/gamemanager.h"
#include "renderengine.h"
#include "QMessageBox"
#include "selectedcardmanager.h"
#include "../graOKod/gamefinishexception.h"
#include "dataparser.h"
#include "../graOKod/game.h"
#include "../graOKod/player.h"
#include "../graOKod/deck.h"
#include "../graOKod/card.h"
#include "../graOKod/cardmanager.h"
#include "../graOKod/cardfactory.h"
#include "connection/servercontroller.h"
#include "../graOKod/deck.h"
#include <curl/curl.h>
#include "json/jsondecoder.h"
#include "deckbuilder.h"
#include "controller/playermovecontroller.h"
#include "exception/postrequestexception.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    MainWindow::showFullScreen();

    ui->gameArea->setVisible(false);
    ui->menu->setVisible(true);
    ui->multiplayerFrame->setVisible(false);
    ui->deckCreator->setVisible(false);

    waitThread = nullptr;
}


MainWindow::~MainWindow() {
    delete ui;
}

//      -------------------------------------------------     MENU      ----------------------------------------------------

void MainWindow::on_hotSeatsButton_clicked() {
    // zmiana widoku
    ui->menu->setVisible(false);
    ui->gameArea->setVisible(true);

    gameMode = HotSeatsMode; // ustawienie trybu gry

    gameManager = new GameManager();
    gameManager->createGame(HotSeats);
    gameManager->getGame()->getPlayerOne()->setName("1");
    gameManager->getGame()->getPlayerTwo()->setName("2");
    gameManager->startGame(); //rozpoczęcie gry

    initRenderEngine(); //inicjalizacja wyświetlania

    playerMoveController = new PlayerMoveController(gameManager, renderEngine);
    playerMoveController->setGameMode(HotSeatsMode);

    // rozpozęcie tury
    gameManager->startTurn();
    SelectedCardManager::getInstance().clearSelected();
    renderEngine->drawActivePlayerTable();
}

void MainWindow::on_multiplayerButton_clicked() {
    if (ui->multiplayerFrame->isVisible()) {
        ui->multiplayerFrame->setVisible(false);
    } else {
        ui->multiplayerFrame->setVisible(true);
    }
}

void MainWindow::on_createGameButton_clicked() {
    std::string nick = ui->nickEdit->text().toStdString();

    if (nick == "") {
        QMessageBox::information(0, QString("Nowa gra"), QString("Wprowadź nick!"));
        return;
    }

    gameMode = MultiplayerMode;
    gameManager = new GameManager;
    gameManager->createGame(CreateMultiplayer);
    gameManager->getGame()->setLocalPlayer(gameManager->getGame()->getPlayerOne());
    gameManager->getGame()->getLocalPlayer()->setName(nick);

    try {
        std::string createGameResponse = ServerController::createGame(nick, gameManager->getGame()->getLocalPlayer()->getStackDeck());
        CreateGameJsonResponse decodedResponse = JsonDecoder::decodeCreateGameJsonResponse(createGameResponse);

        gameManager->getGame()->getLocalPlayer()->setId(decodedResponse.getPlayerId());
        gameManager->getGame()->setId(decodedResponse.getGameId());

        QMessageBox::information(0, QString("Gra o Kod"), QString("Stworzono grę o ID: " + QString::number(decodedResponse.getGameId()) + ". Oczekiwanie na przeciwnika."));

        std::string joinedOpponentDataResponse = ServerController::waitForOpponentJoin(gameManager->getGame());
        JoinedOpponentJsonResponse decodedJoindedOpponentJsonResponse = JsonDecoder::decodeJoinedOpponentJsonResponse(joinedOpponentDataResponse);

        gameManager->joinGame(CreateMultiplayer,
                              decodedJoindedOpponentJsonResponse.getOpponentId(),
                              decodedJoindedOpponentJsonResponse.getOpponentName(),
                              decodedJoindedOpponentJsonResponse.getOpponentDeck());
    } catch (PostRequestException& ex) {
        QMessageBox::information(0, QString("Błąd serwera"), QString("Nie można utworzyć nowej gry"));
        return;
    } catch (std::exception& exception) {
        QMessageBox::information(0, QString("Nowa gra: błąd"), QString::fromLocal8Bit(exception.what()));
        return;
    }

    QMessageBox::information(0, QString("Gra o Kod"),  QString("Gracz dołączył do gry! Startujemy!"));

    gameManager->startGame(); //rozpoczęcie gry

    initRenderEngine(); //inicjalizacja wyświetlania

    playerMoveController = new PlayerMoveController(gameManager, renderEngine);
    playerMoveController->setGameMode(MultiplayerMode);

    // rozpozęcie tury
    ui->menu->setVisible(false);
    ui->gameArea->setVisible(true);

    gameManager->startTurn();

    SelectedCardManager::getInstance().clearSelected();
    renderEngine->drawLocalPlayerTable();

    //stworzenie watku
    waitThread = new WaitActionThread;
    waitThread->setGame(gameManager->getGame());
    connect(waitThread, SIGNAL(gameUpdate(int,int,int)), this, SLOT(receivedAction(int,int,int)));
}

void MainWindow::on_connectToGameButton_clicked() {
    std::string nick = ui->nickEdit->text().toStdString();
    if ("" == nick) {
        QMessageBox::information(0, QString("Nowa gra"), QString("Wprowadź nick!"));
        return;
    }

    int gameId = ui->gameIdEdit->text().toInt();

    gameMode = MultiplayerMode;
    gameManager = new GameManager;
    gameManager->createGame(JoinMultiplayer);
    gameManager->getGame()->setLocalPlayer(gameManager->getGame()->getPlayerTwo());
    gameManager->getGame()->getLocalPlayer()->setName(nick);

    try {
        std::string joinGameResponse = ServerController::joinGame(nick, gameId, gameManager->getGame()->getLocalPlayer()->getStackDeck());
        JoinGameJsonResponse decodedJoinGameResponse = JsonDecoder::decodeJoinGameJsonResponse(joinGameResponse);
        gameManager->joinGame(
            JoinMultiplayer,
            decodedJoinGameResponse.getOpponentId(),
            decodedJoinGameResponse.getOpponentName(),
            decodedJoinGameResponse.getOpponentDeck(),
            decodedJoinGameResponse.getGameId(),
            decodedJoinGameResponse.getPlayerId()
        );
    } catch (PostRequestException& ex) {
        QMessageBox::information(0, QString("Błąd serwera"), QString("Nie można dołączyć do gry"));
        return;
    }




    QMessageBox::information(0, QString("Gra o Kod"),  QString("Dołączono do gry! Startujemy!"));
    gameManager->startGame(); //rozpoczęcie gry

    initRenderEngine(); //inicjalizacja wyświetlania

    playerMoveController = new PlayerMoveController(gameManager, renderEngine);
    playerMoveController->setGameMode(MultiplayerMode);

    // rozpozęcie tury
    ui->menu->setVisible(false);
    ui->gameArea->setVisible(true);

    gameManager->startTurn();
    SelectedCardManager::getInstance().clearSelected();
    renderEngine->drawLocalPlayerTable();

    //stworzenie watku
    waitThread = new WaitActionThread;
    waitThread->setGame(gameManager->getGame());
    connect(waitThread, SIGNAL(gameUpdate(int,int,int)), this, SLOT(receivedAction(int,int,int)));
    waitThread->start();

    //wylaczenie obslugi przyciwków
    ui->actionButtonsFrame->setEnabled(false);
}

void MainWindow::on_deckBuilderButton_clicked() {
    ui->menu->setVisible(false);
    ui->deckCreator->setVisible(true);
    deckBuilder = new DeckBuilder(this);
    deckBuilder->setCardListTable(ui->cardTable);
    deckBuilder->setCountLabel(ui->cardCountLabel);
    deckBuilder->drawCardListTable();
    connect(ui->cardTable, SIGNAL(cellClicked(int,int)), deckBuilder, SLOT(cellSelected(int,int)));
}

void MainWindow::on_QuitButton_clicked() {
    QApplication::quit();
}

//      -------------------------------------------     ROZGRYWKA      ------------------------------------------------------

void MainWindow::on_playCardButton_clicked() {
    try {
        playerMoveController->playCard();
    } catch (GameFinishException& ex) {
        // złapanie wyjatku końca gry wyświetla informacje o zwyciezcy i przechodzi do widoku menu
        QMessageBox::information(0, QString::fromLocal8Bit(ex.what()), QString::fromStdString(ex.winnerName()));
        endGame();
    }
}

void MainWindow::on_attackCreatureButton_clicked() {
    try {
        playerMoveController->attackCreature();
    } catch (GameFinishException& ex) {
        // złapanie wyjatku końca gry wyświetla informacje o zwyciezcy i przechodzi do widoku menu
        QMessageBox::information(0, QString::fromLocal8Bit(ex.what()), QString::fromStdString(ex.winnerName()));
        endGame();
    }
}

void MainWindow::on_attackPlayerButton_clicked() {
    try {
        playerMoveController->attackPlayer();
    } catch (GameFinishException& ex) {
        // złapanie wyjatku końca gry wyświetla informacje o zwyciezcy i przechodzi do widoku menu
        QMessageBox::information(0, QString::fromLocal8Bit(ex.what()), QString::fromStdString(ex.winnerName()));
        endGame();
    }
}

void MainWindow::on_endTurnButton_clicked() {
    try {
        playerMoveController->endTurn();
    } catch (GameFinishException& ex) {
        // złapanie wyjatku końca gry wyświetla informacje o zwyciezcy i przechodzi do widoku menu
        QMessageBox::information(0, QString::fromLocal8Bit(ex.what()), QString::fromStdString(ex.winnerName()));
        endGame();
    }

    if (gameMode == MultiplayerMode) {
        ui->actionButtonsFrame->setEnabled(false);
        waitThread->start();
    }
}

void MainWindow::on_surrenderButton_clicked() {
    try {
        playerMoveController->surrender();
    } catch (GameFinishException& ex) {
        // złapanie wyjatku końca gry wyświetla informacje o zwyciezcy i przechodzi do widoku menu
        QMessageBox::information(0, QString::fromLocal8Bit(ex.what()), QString::fromStdString(ex.winnerName()));
        endGame();
    }
}

//      ------------------------------------------------     KREATOR TALII      ----------------------------------------------------

void MainWindow::on_deckCreatorBackButton_clicked() {
    ui->menu->setVisible(true);
    ui->deckCreator->setVisible(false);
    delete deckBuilder;
}

void MainWindow::on_addCardToDeckButton_clicked() {
    deckBuilder->addCardToDeck();
}

void MainWindow::on_removeCardFromDeckButton_clicked() {
    deckBuilder->removeCardFromDeck();
}

void MainWindow::on_saveDeckButtton_clicked() {
    try {
        deckBuilder->saveToFile();
        QMessageBox::information(0, QString("Kreator talii"), QString("Zapisano talie!"));
        on_deckCreatorBackButton_clicked();
    } catch (std::exception& ex) {
        QMessageBox::information(0, QString("Błąd"), QString::fromLocal8Bit(ex.what()));
    }
}


//      -------------------------------------------------     HELPERY      -----------------------------------------------------------

void MainWindow::endGame() {
    gameManager->endGame();
    SelectedCardManager::getInstance().clearSelected();

    delete renderEngine;
    delete gameManager;
    delete waitThread;
    delete playerMoveController;

    ui->actionButtonsFrame->setEnabled(true);
    ui->gameArea->setVisible(false);
    ui->menu->setVisible(true);
}

void MainWindow::receivedAction(int actionType, int cardId, int targetId) {
    waitThread->pause();

    Card* card = getCardById(cardId);
    Card* target = getCardById(targetId);

    try {
        switch (actionType) {
        case playCardAction:
            gameManager->activePlayerPlayCard(card, target);
            break;
        case attackCreatureAction:
            gameManager->attackCreature(card,target);
            break;
        case attackPlayerAction:
            gameManager->attackPlayer(card);
            break;
        case endTurnAction:
            gameManager->endTurnActions();
            gameManager->endTurn();
            gameManager->startTurn();
            gameManager->startTurnActions();
            ui->actionButtonsFrame->setEnabled(true);
            waitThread->stop();
            SelectedCardManager::getInstance().clearSelected();
            renderEngine->drawLocalPlayerTable();
            break;
        case surrenderAction:
            gameManager->activePlayerSurrender();
            break;
        default:
            break;
        }
    } catch (GameFinishException& ex){
        QMessageBox::information(0, QString::fromLocal8Bit(ex.what()), QString::fromStdString(ex.winnerName()));
        waitThread->stop();
        while (waitThread->isRunning());
        endGame();
        return;
    }
    SelectedCardManager::getInstance().clearSelected();
    renderEngine->drawLocalPlayerTable();

    waitThread->resume();
}

Card *MainWindow::getCardById(int id) {
    if (id == 0) {
        return nullptr;
    }

    cardManager::getCardById(id, gameManager->getGame());
}

void MainWindow::initRenderEngine()
{
    renderEngine = new RenderEngine(
        gameManager->getGame(),
        ui->playerHandLayout,
        ui->playerTableLayout,
        ui->enemyTableLayout,
        ui->playerInfo,
        ui->enemyInfo
    );
}
