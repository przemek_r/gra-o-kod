#include "playermovecontroller.h"
#include "selectedcardmanager.h"
#include "QMessageBox"
#include "connection/servercontroller.h"
#include "renderengine.h"
#include "dataparser.h"
#include "../graOKod/game.h"
#include "../graOKod/gamemanager.h"
#include "../graOKod/gamefinishexception.h"

#include <exception>

PlayerMoveController::PlayerMoveController(GameManager* _gameManager, RenderEngine* _renderEngine)
{
    gameManager = _gameManager;
    renderEngine = _renderEngine;
}

PlayerMoveController::~PlayerMoveController() {}

void PlayerMoveController::setGameMode(GameMode _gameMode)
{
    gameMode = _gameMode;
}

void PlayerMoveController::playCard()
{
    try {
        // sprawdzenie czy cel jest wrogi czy przyjazny
        if (SelectedCardManager::getInstance().getSelectedTargetCard() != nullptr) {
            gameManager->activePlayerPlayCard(
                SelectedCardManager::getInstance().getSelectedHandCard(),
                SelectedCardManager::getInstance().getSelectedTargetCard()
            );

            if (gameMode == MultiplayerMode) {
                ServerController::makeAction(
                    playCardAction,
                    gameManager->getGame()->getLocalPlayer(),
                    SelectedCardManager::getInstance().getSelectedHandCard(),
                    SelectedCardManager::getInstance().getSelectedTargetCard()
                );
            }
        } else {
            gameManager->activePlayerPlayCard(
                SelectedCardManager::getInstance().getSelectedHandCard(),
                SelectedCardManager::getInstance().getSelectedTableCard()
            );

            if (gameMode == MultiplayerMode) {
                ServerController::makeAction(
                    playCardAction,
                    gameManager->getGame()->getLocalPlayer(),
                    SelectedCardManager::getInstance().getSelectedHandCard(),
                    SelectedCardManager::getInstance().getSelectedTableCard()
                );
            }
        }
    } catch (GameFinishException& ex) {
        throw ex; // obsłużone w MainWindow
    } catch (std::exception& ex) {
        // jesli złapano inny wyjatek, to wyswietla go na ekranie
        QMessageBox::information(0, "Niepopawna akcja", QString::fromLocal8Bit(ex.what()));
    }

    // aktualizacja wyświetlania
    SelectedCardManager::getInstance().clearSelected();
    drawPlayerTable();
}

void PlayerMoveController::attackCreature()
{
    try {
        gameManager->attackCreature(
            SelectedCardManager::getInstance().getSelectedTableCard(),
            SelectedCardManager::getInstance().getSelectedTargetCard()
        );

        if (gameMode == MultiplayerMode) {
            ServerController::makeAction(
                attackCreatureAction,
                gameManager->getGame()->getLocalPlayer(),
                SelectedCardManager::getInstance().getSelectedTableCard(),
                SelectedCardManager::getInstance().getSelectedTargetCard()
            );
        }
    } catch (GameFinishException& ex) {
        throw ex; // obsłużone w MainWindow
    } catch (std::exception& ex) {
        // jesli złapano inny wyjatek, to wyswietla go na ekranie
        QMessageBox::information(0, "Niepopawna akcja", QString::fromLocal8Bit(ex.what()));
    }

    // aktualizacja wyświetlania
    SelectedCardManager::getInstance().clearSelected();
    drawPlayerTable();
}

void PlayerMoveController::attackPlayer()
{
    try {
        gameManager->attackPlayer(SelectedCardManager::getInstance().getSelectedTableCard());

        if (gameMode == MultiplayerMode) {
            ServerController::makeAction(
                attackPlayerAction,
                gameManager->getGame()->getLocalPlayer(),
                SelectedCardManager::getInstance().getSelectedTableCard(),
                nullptr
            );
        }
    } catch (GameFinishException& ex) {
        throw ex; // obsłużone w MainWindow
    } catch (std::exception& ex) {
        // jesli złapano inny wyjatek, to wyswietla go na ekranie
        QMessageBox::information(0, "Niepopawna akcja", QString::fromLocal8Bit(ex.what()));
    }

    // aktualizacja wyświetlania
    SelectedCardManager::getInstance().clearSelected();
    drawPlayerTable();
}

void PlayerMoveController::endTurn()
{
    if (gameMode == MultiplayerMode) {
        // akcje na zakończenie tury
        ServerController::makeAction(endTurnAction, gameManager->getGame()->getLocalPlayer(), nullptr, nullptr);
    }

    gameManager->endTurnActions();
    gameManager->endTurn();
    gameManager->startTurn();
    gameManager->startTurnActions();

    // aktualizacja wyświetlania
    SelectedCardManager::getInstance().clearSelected();
    drawPlayerTable();
}

void PlayerMoveController::surrender()
{
    try {
        gameManager->activePlayerSurrender();
    } catch (GameFinishException& ex) {
        if (gameMode == MultiplayerMode) {
            ServerController::makeAction(surrenderAction,
                gameManager->getGame()->getLocalPlayer(),
                nullptr,
                nullptr
            );
        }

        throw ex; // Obsłużone w MainWindow
    } catch (std::exception& ex) {
        QMessageBox::information(0, "Niepopawna akcja", QString::fromLocal8Bit(ex.what()));
    }

    SelectedCardManager::getInstance().clearSelected();
    drawPlayerTable();
}

void PlayerMoveController::drawPlayerTable()
{
    if (gameMode == MultiplayerMode) {
        renderEngine->drawLocalPlayerTable();
    } else {
        renderEngine->drawActivePlayerTable();
    }
}
