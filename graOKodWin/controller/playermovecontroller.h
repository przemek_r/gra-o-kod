#ifndef PLAYERMOVECONTROLLER_H
#define PLAYERMOVECONTROLLER_H

#include "../graOKod/gamemanager.h"
#include "renderengine.h"

/**
 * @brief Klasa do kontroli ruchów gracz.
 */
class PlayerMoveController {
public:
    /**
     * @brief Konstruktor PlayerMoveController
     * @param gameManager
     * @param renderEngine
     */
    PlayerMoveController(GameManager* gameManager, RenderEngine* renderEngine);
    ~PlayerMoveController();

    /**
     * @brief Zagranie karty.
     */
    void playCard();

    /**
     * @brief Atak na kreaturę.
     */
    void attackCreature();

    /**
     * @brief Atak na gracza.
     */
    void attackPlayer();

    /**
     * @brief Koniec tury.
     */
    void endTurn();

    /**
     * @brief Poddanie się.
     */
    void surrender();

    void setGameMode(GameMode gameMode);
protected:
    GameManager* gameManager;
    RenderEngine* renderEngine;
    GameMode gameMode;

    /**
     * @brief Wyświetlenie stołu gracza.
     */
    void drawPlayerTable();
};

#endif // PLAYERMOVECONTROLLER_H
