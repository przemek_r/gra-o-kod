#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "../graOKod/gamemanager.h"
#include "waitactionthread.h"
#include "controller/playermovecontroller.h"

class RenderEngine;
class DeckBuilder;

namespace Ui {
class MainWindow;
}

/**
 * @brief Główne okno aplikacji.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    RenderEngine* renderEngine;
    GameManager* gameManager;
    DeckBuilder* deckBuilder;
    WaitActionThread* waitThread;
    PlayerMoveController* playerMoveController;
    GameMode gameMode;

    /**
     * @brief Metoda kończąca rozgrywkę.
     */
    void endGame();

    /**
     * @brief Metoda pobierająca kartę na podstawie id.
     * @param id
     * @return
     */
    Card* getCardById(int id);

    /**
     * @brief Inicjalizuje obiekt renderEngine
     */
    void initRenderEngine();

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    /**
     * @brief Zagrywa kartę.
     */
    void on_playCardButton_clicked();

    /**
     * @brief Atakuje kreaturę.
     */
    void on_attackCreatureButton_clicked();

    /**
     * @brief Atakuje gracza.
     */
    void on_attackPlayerButton_clicked();

    /**
     * @brief Kończy turę.
     */
    void on_endTurnButton_clicked();

    /**
     * @brief Zaczyna grę w trybie 'gorące krzesła'.
     */
    void on_hotSeatsButton_clicked();

    /**
     * @brief Zamyka aplikację.
     */
    void on_QuitButton_clicked();

    /**
     * @brief Poddaje grę.
     */
    void on_surrenderButton_clicked();

    /**
     * @brief Otwiera okno trybu gry wieloosobowej.
     */
    void on_multiplayerButton_clicked();

    /**
     * @brief Tworzy grę wieloosobową.
     */
    void on_createGameButton_clicked();

    /**
     * @brief Dołącza do gry wieloosobowej.
     */
    void on_connectToGameButton_clicked();

    /**
     * @brief Uruchamia kreatora talii.
     */
    void on_deckBuilderButton_clicked();

    /**
     * @brief Powraca do menu z kreatora talii.
     */
    void on_deckCreatorBackButton_clicked();

    /**
     * @brief Dodaje kartę do talii.
     */
    void on_addCardToDeckButton_clicked();

    /**
     * @brief Usuwa kartę z talii.
     */
    void on_removeCardFromDeckButton_clicked();

    /**
     * @brief Zapisuje talie.
     */
    void on_saveDeckButtton_clicked();

    /**
     * @brief Metoda wywoływana, gdy odebrany zostanie sygnał o odebraniu akcji od przeciwnika.
     * @param actionType
     * @param cardId
     * @param targetId
     */
    void receivedAction(int actionType, int cardId, int targetId);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H

/*! \mainpage Dokumentacja techniczna Gry o Kod
 *
 * \section intro_sec I. Wstęp
 *
 * Aplikacja Gra o Kod powstała w wyniku realizacji projektu na przedmiot Programowanie Systemów Sterowania.
 *
 * Autorami aplikacji są:
 * - Paweł Hertman
 * - Przemysław Recha
 * - Tomasz Maryńczuk
 *
 * Celem projektu było stworzenie karcianej gry komputerowej wzorowanej na kolekcjonerskich grach karcianych. Gra w założeniach pozwalała na rozgrywkę dwóch osób z użyciem jednego
 * komputera, tzw. "Gorące krzesła", jak i rozgrywkę przez sieć. Z uwagi na przedmiot Programowanie Systemów Sterowania gra utrzymuje konwencję programistyczną.
 *
 * \section devel_sec II. Aplikacja
 *
 * \subsection tech_sec 1. Technologie
 *
 * \subsection server_sec a) Serwer
 *
 * Serwer API odpowiada za tworzenie gry i jej obsługę pomiędzy graczami. Został zrealizowany w języku PHP w oparciu o
 * framework MVC Symfony2 z wykorzystaniem biblioteki ORM (mapowanie obiektowo-relacyjne) Doctrine do obsługi bazy danych
 * PostgreSQL. Aplikacja API także została zrealizowana zgodnie z paradygmatem OOP.
 *
 * \subsection client_sec b) Klient
 *
 * Klient został napisany w język C++. Do stworzenia aplikacji wykorzystaliśmy IDE QtCreator w celu wykorzystania możliwości zaprojektowania interfejsu graficznego i skorzystania z klas Qt.
 * Do komunikacji z serwerem wykorzystujemy bibliotęke CURL. Do analizy formatu JSON wykorzystaliśmy bibliotekę Rapidjson.
 *
 * \image html game1.png "Widok rozgrywki"
 *
 * \subsection effect_sec 2. Działanie aplikacji
 *
 * Aplikacja uruchamiana jest w menu, z którego użytkowanik ma nastepujące opcje do wyboru:
 * - Gorące krzesła (gra na jednym komputerze)
 * - Gra przez sieć
 * - Kreator talii
 * - Wyjście
 *
 * \image html menu.png "Menu gry"
 *
 * Klasa MainWindow odpowiedzialna jest za wyświetlanie (przy użyciu RenderEngine), za kontrolę rozgrywki (przy użyciu PlayerMoveController i GameManager), oraz za komunikacje z serwerem (przy użyciu ServerController).
 *
 * \subsection game_sec a) Rozgrywka
 *
 * Gracz ma do wyboru dwa tryby rozgrywki: "Gorące krzesła" oraz "Gra przez sieć".
 *
 * Gdy użytkownik wybierze "Gorące krzesła" od razu następuję stworzenie gry przez klasę GameManager i wyświetlenie pola gry przez RenderEngine.
 * ServerController i WaitActionThread nie są używane w tym trybie gry.
 *
 * W przypadku wyboru gry przez sieć użytkownik posiada dwie opcje: "Stwórz grę" i "Dołącz do gry". W przypadku wyboru "Stwórz grę" klient wysyła do serwera API zapytanie HTTP typu POST z danymi do stworzenia gry. W odpowiedzi klient otrzymuje dane gry. Następnie wysyła zapytanie typu
 * GET sprawdzające czy do gry dołączył drugi gracz. Gry drugi gracz dołączy do gry klient otrzymuje informacje o drugim graczu (identyfikator, pseudonim, talia). Po otrzymaniu tych infromacji następuje
 * inicjalizacja elementów wymaganych do prowadzenia rozgrywki: RenderEngine, WaitActionThread oraz wyświetleni widoku rozgrywki.
 *
 * W przypadku wybrania "Gra przez sieć", klient wysyła zapytanie HTTP typu POST z danymi gracza, i jeśli istenieje gra o podanym identyfikatorze, to otrzymuje w odpowiedzi dane gracza, który założył grę.
 * Po otrzymaniu tych infromacji następuje inicjalizacja gry, tak jak w poprzednim akapicie. Dodatkowo następuje uruchomienie wątku oczekującego na akcje przeciwnika.
 * W przypadku nie otrzymania danych następuję wyświetlenie komunikatu błędu.
 *
 * Wysyłanie odpowiednich zapytań jest obsługiwane przez ServerController.
 *
 * \image html game2.png "Pole gry"
 *
 * \image html karta.png "Widok karty"
 *
 * Wyświetlanie karty realizowane jest przez klasę CardWidget, która dziedziczy po QWidget. Dziedziczenie po QWidget pozwala na przełodowanie metod paintEvent() i mousePressEvent(), które realizują
 * rysowanie karty i zaznaczanie karty przy użyciu myszy. Informacje o wybranej karcie przechowywane są w obiekcie SelectedCardManager.
 *
 * Informacje o karcie przechowywane są w obiekcie Card. Obiekt ten zawiera informację o koszcie, życiu, ataku, opisie, pozwala na modyfikacje tych danych. Każda karta reprezentowana jest
 * przez konretną klasę dziedzicząca po klasie Card. W konstruktorze każdej konretnej karty następuje inicjalizacja pól klasy przechowujących informacje o karcie. W przypadku, gdy karta
 * posiada akcję przy zagraniu, ataku, zniszczeniu, na koniec lub początek tury, należy przeładować odpowiednią metodę w tej klasie obsługującą daną akcję.
 *
 * Użytkownik podczas rozgrywki ma do wyboru pięć akcji:
 * - Zagraj kartę
 * - Zaatakuj kreaturę
 * - Zaatakuj gracza
 * - Zakończ turę
 * - Poddaj się
 *
 * Po wybraniu kart i wciśnięcia przycisku akcji następuje wywołanie odpowiedniej metody klasy PlayerMoveController. Do obsługi akcji wykorzystywana jest klasa ActionManager, która korzysta z wzorca projektowego
 * "Łańcuch zobowiązań". W przypadku, gdy wykonanie akcji jest niemożliwe, zostaję wyrzucony wyjątek. Informacja o tym wyświetlana jest użytkownikowi. Akcje dodawane są do kolejki akcji, które są
 * kolejno rozpatrywane, np.: Gracz wybiera akcję "Zaatakuj kreaturę" do kolejki akcji zostaje dodana akcja zaatakowania. Podczas obsługi tej akcji przez odpowiednią metodę kalsy dziedziczącej po ActionHandler.
 * W pierwszej kolejności sprawdzane są wszystkie warunki czy wykonanie akcji jest możliwe, jeśli nie następuje wyrzucenie wyjątku i wyczyszczenie kolejki akcji. Jeśli wszystkie warunki zostaną spełnione,
 * to nasępuje dodanie do łańcucha akcji kolejnych akcji: zadanie obrażeń, sprawdzenie śmierci kreatur, sprawdzenie śmierci graczy. Po zakończeniu obsługi akcji, następuje wykonywanie kolejnych akcji
 * w kolejce. Obsłużenie wcześniej dodanych akcji, może spowodować dodanie kolejnych akcji do łańcucha, przykładowo gdy karta zostanie zniszczona i posiada akcję przy zniszczeniu, która doda kolejną
 * akcję do kolejki.
 *
 * GameManager wykorzystywany jest zarówno w trybie rozgrywki lokalnej, jak i sieciowej. Tryb sieciowy, różni się tym, że dodatkowo wykorzystywany jest ServerController, kóry wysyła odpowiednie zapytania
 * na serwer API. Zapytania te zawierają informacje o typie akcji, i wybranych kartach do wykoania akcji. Klient, który zakończył turę przechodzi w stan oczekiwania na akcję przeciwnika. Oczekiwanie
 * realizowane jest przez wątek WaitActionThread. Wątek wysyła zapytanie typu GET sprawdzające czy akcja została wykonana. Gdy otrzyma informacje, emituje sygnał updateGame(), który powoduje wykonanie przez
 * MainWindow metody receivedAction().
 *
 * W momencie śmierci lub poddania się gracza rozgrywka jest kończona i następuje powrót do menu.
 *
 * \section deckBuilder_sec b) Kreator talii
 *
 * \image html deckBuilder.png "Kreator talii"
 *
 * Kreator talii pozwala spersonalizować talię gracza. Po wybraniu z menu "Kreator talii" następuje stworzenie obiektu klasy DeckBuilder, która jest odpowiedzialna za obsługę tworzenia talii, i wyświetlenie
 * widoku kreatora talii. Użytkownik wybierając kartę z listy i używając przycisków "Dodaj kartę" i "Usuń kartę", może personalizować własną talię. Po naciśnięciu przycisku następuje zapisanie talii do pliku
 * i powrót do menu.
 *
 * \section end_sec III. Mowa końcowa
 *
 * Projekt udało się zrealizować zgodnie z założeniami. Jesteśmy zadowoleni z uzyskanych efektów, ale jesteśmy świadomi niedoskonałości
 * własnego warsztatu i potrzebę samokształcenia w zakresie programowania obiektowego. Aplikacja wymaga przede wszystkim stworzenia
 * oprawy wizualnej.
 *
 * Instrukcja do gry znjduję się <a href="instrukcja.pdf">tutaj</a>.
 *
 * Diagram UML znajduje się <a href="diagram-uml.html">tutaj</a>.
 *
 */


