#include "servercontroller.h"
#include "curlexecutor.h"
#include "dataparser.h"
#include "../graOKod/game.h"
#include "utils/stringchararrayconverter.h"
#include <pthread.h>

std::string const ServerController::API_URL = "http://desolate-mountain-9763.herokuapp.com";

std::string ServerController::createGame(std::string creatorNick, Deck *creatorDeck)
{
    char *data = StringCharArrayConverter::stringToCharArray(
        DataParser::createGameJsonData(creatorNick, creatorDeck)
    );
    std::string url = ServerController::API_URL +  "/game/create";

    std::string response = CurlExecutor::sendPostRequest(url, data);

    return response;
}

std::string ServerController::joinGame(std::string playerNick, int gameId, Deck *joinDeck)
{
    char *data = StringCharArrayConverter::stringToCharArray(
        DataParser::joinGameJsonData(playerNick, gameId, joinDeck)
    );
    std::string url = ServerController::API_URL + "/game/join";

    std::string response = CurlExecutor::sendPostRequest(url, data);

    return response;
}

std::string ServerController::waitForOpponentJoin(Game* game)
{
    std::string response = "no opponent";
    std::string url = ServerController::API_URL + "/game/check-for-opponent/" + std::to_string(game->getId());

    while (response == "no opponent") {
        // @todo zrobić to na wątkach albo umożliwić jakoś przerwanie oczekiwania przeciwnikana
        response = CurlExecutor::sendGetRequest(url);
    }

    return response;
}

std::string ServerController::makeAction(ActionType actionType, Player *player, Card *card, Card *target)
{
    char *data = StringCharArrayConverter::stringToCharArray(
        DataParser::makeActionsJsonData(player->getId(), actionType, card, target)
    );
    std::string url = ServerController::API_URL + "/game/make-actions";

    std::string response = CurlExecutor::sendPostRequest(url, data);

    return response;
}

std::string ServerController::checkForOpponentActions(Game *game)
{
    std::string url = ServerController::API_URL + "/game/check-for-opponent-actions/"
        + std::to_string(game->getId()) + "/"
        + std::to_string(game->getOtherPlayer(game->getLocalPlayer())->getId());

    std::string response = CurlExecutor::sendGetRequest(url);

    return response;
}
