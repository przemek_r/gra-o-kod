#include "curlexecutor.h"
#include "exception"
#include <curl/curl.h>
#include "exception/postrequestexception.h"
#include "utils/stringchararrayconverter.h"


size_t CurlExecutor::writeCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);

    return size * nmemb;
}

std::string CurlExecutor::sendPostRequest(std::string url, char *data)
{
    CURL *curl;
    CURLcode res;
    std::string readBuffer;

    curl = curl_easy_init();
    if (curl) {
      curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
      curl_easy_setopt(curl, CURLOPT_URL, StringCharArrayConverter::stringToCharArray(url));
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CurlExecutor::writeCallback);
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
      res = curl_easy_perform(curl);
      curl_easy_cleanup(curl);

      long http_code = 0;
      curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
      if (http_code != 200 || res == CURLE_ABORTED_BY_CALLBACK) {
          throw PostRequestException();
      }

      return readBuffer;
    }


    throw std::exception();
}

std::string CurlExecutor::sendGetRequest(std::string url)
{
    CURL *curl;
    CURLcode res;
    std::string readBuffer;

    curl = curl_easy_init();
    if (curl) {
      curl_easy_setopt(curl, CURLOPT_URL, StringCharArrayConverter::stringToCharArray(url));
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CurlExecutor::writeCallback);
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
      res = curl_easy_perform(curl);
      curl_easy_cleanup(curl);

      long http_code = 0;
      curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
      if (http_code != 200 || res == CURLE_ABORTED_BY_CALLBACK) {
          throw PostRequestException();
      }

      return readBuffer;
    }


    throw std::exception();
}

