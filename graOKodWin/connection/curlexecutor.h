#ifndef CURLEXECUTOR_H
#define CURLEXECUTOR_H

#include <string>

/**
 * @brief Klasa obsługuje wywołania curl.
 *
 */
class CurlExecutor {
private:
    CurlExecutor() {}
public:
    /**
     * @brief Wysłanie zapytania Post.
     * @param url
     * @param data
     * @return
     */
    static std::string sendPostRequest(std::string url, char* data);

    /**
     * @brief Wysłanienie zapytania Get.
     * @param url
     * @return
     */
    static std::string sendGetRequest(std::string url);

protected:

    static size_t writeCallback(void *contents, size_t size, size_t nmemb, void *userp);
};

#endif // CURLEXECUTOR_H
