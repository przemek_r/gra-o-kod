#ifndef SERVERCONTROLLER_H
#define SERVERCONTROLLER_H
#include <string>
#include "../dataparser.h"

class Action;
class Deck;
class Game;
class Player;


/**
 * @brief Klasa obsługująca wysyłąnie i odbieranie zapytań w grze przez sieć.
 *   Hertma
 *  
 */
class ServerController {
private:
    ServerController() {}
    static const std::string API_URL;

public:
    /**
     * @brief Wysłanie zapytania stworzenia gry.
     * @param creatorNick
     * @param creatorDeck
     * @return
     */
    static std::string createGame(std::string creatorNick, Deck *creatorDeck);

    /**
     * @brief Wysłanie zapytania dołączenia do gry.
     * @param playerNick
     * @param gameId
     * @param joinDeck
     * @return
     */
    static std::string joinGame(std::string playerNick,int gameId ,Deck *joinDeck);

    /**
     * @brief Wysłanie zapytania o dołączenie gracza.
     * @param game
     * @return
     */
    static std::string waitForOpponentJoin(Game *game);

    /**
     * @brief Wysłanie zapytania a akcją.
     * @param actionType
     * @param player
     * @param card
     * @param target
     * @return
     */
    static std::string makeAction(ActionType actionType, Player* player, Card* card, Card* target);

    /**
     * @brief wysłanie zapytania o akcje przeciwnika.
     * @param game
     * @return
     */
    static std::string checkForOpponentActions(Game *game);
};

#endif // SERVERCONTROLLER_H
