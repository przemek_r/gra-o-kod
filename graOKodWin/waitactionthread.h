#ifndef WAITACTIONTHREAD_H
#define WAITACTIONTHREAD_H
#include <QThread>
#include <QMutexLocker>

class Game;

/**
 * @brief Wątek oczekujący na akcję przeciwnika.
 */
class WaitActionThread : public QThread {
    Q_OBJECT
private:
    Game* game;
    bool stoped;
    bool paused;
    QMutex mutex;

    void run();
    /**
     * @brief Sprawdzanie akcji
     */
    void checkForAction();
public:
    WaitActionThread();

    void setGame(Game *game_);

    /**
     * @brief Wstrzymuje działanie wątku.
     */
    void pause();

    /**
     * @brief Wznawia działanie wątku.
     */
    void resume();

    /**
     * @brief Zatrzymuje wątek.
     */
    void stop();

signals:
    /**
     * @brief Sygnał emitowany przez wątek, gdy odbierze akcje. Powinien wywołać funkje aktualizującą stan gry i wyświetlanie.
     * @param actionType
     * @param cardId
     * @param targetId
     */
    void gameUpdate(int actionType, int cardId, int targetId);
public slots:
};

#endif // WAITACTIONTHREAD_H
