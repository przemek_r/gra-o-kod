#include "dataparser.h"
#include "../graOKod/deck.h"
#include "string"
#include "../graOKod/card.h"

std::string DataParser::cardToJson(Card *card) {
    std::string result;
    result = "{\"id\": " + std::to_string(card->getId()) + ", \"cardTypeId\": " + std::to_string(card->getSetId()) + "}";
    return result;
}

std::string DataParser::deckToJson(Deck *deck) {
    std::string result = "";
    for (CardVector::iterator it = deck->getCards().begin(); it != deck->getCards().end(); ++it) {
        result += "cards[]=" + cardToJson(*it) + "&";
    }

    return result;
}

std::string DataParser::createGameJsonData(std::string nick, Deck *deck) {
    std::string result;
    result = "creatorNick=" + nick + "&" + deckToJson(deck);

    return result;
}

std::string DataParser::joinGameJsonData(std::string nick, int gameId, Deck *deck) {
    std::string result;
    result = "playerNick=" + nick + "&" + deckToJson(deck) + "gameId=" + std::to_string(gameId) + "&";

    return result;
}

std::string DataParser::makeActionsJsonData(int playerId, ActionType actionType, Card *card, Card *target) {
    std::string result;
    int cardId;
    int targetId;

    result = "playerId=" + std::to_string(playerId) + "&" +
             "actions[]={\"typeId\": " + std::to_string(actionType);

    if (card != nullptr) {
        result += ",\"cardId\": " + std::to_string(card->getId());
        cardId = card->getId();
    } else {
        result += ",\"cardId\": 0";
        cardId = 0;
    }

    if (target != nullptr) {
        result += ",\"targetCardId\": " + std::to_string(target->getId());
        targetId = target->getId();
    } else {
        result += ",\"targetCardId\": 0";
        targetId = 0;
    }

    result += "}&";

    return result;
}
