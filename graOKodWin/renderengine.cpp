#include "renderengine.h"
#include "../graOKod/game.h"
#include "../graOKod/deck.h"
#include "../graOKod/player.h"
#include "../graOKod/card.h"
#include "QBoxLayout"
#include "QLabel"
#include "cardwidget.h"

void RenderEngine::drawPlayerHand(Player *player) {
    for (
        CardVector::iterator it = player->getHandDeck()->getCards().begin();
        it != player->getHandDeck()->getCards().end();
        ++it
    ) {
        CardWidget* cardWidget = new CardWidget;
        cardWidget->setCard((*it));
        cardWidget->setDeck(PlayerHandDeck);
        playerHandLayout->addWidget(cardWidget);
    }
}

void RenderEngine::drawPlayerTable(Player *player) {
    for (
        CardVector::iterator it = player->getTableDeck()->getCards().begin();
        it != player->getTableDeck()->getCards().end();
        ++it
    ) {
        CardWidget* cardWidget = new CardWidget;
        cardWidget->setCard((*it));
        cardWidget->setDeck(PlayerTableDeck);
        playerTableLayout->addWidget(cardWidget);
    }
}

void RenderEngine::drawEnemyTable(Player *player) {
    for (
        CardVector::iterator it = player->getTableDeck()->getCards().begin();
        it != player->getTableDeck()->getCards().end();
        ++it
    ) {
        CardWidget* cardWidget = new CardWidget;
        cardWidget->setCard((*it));
        cardWidget->setDeck(EnemyTableDeck);
        enemyTableLayout->addWidget(cardWidget);
    }
}

void RenderEngine::drawPlayerInfo(Player *player) {
    playerInfo->setText("Życie gracza: " + QString::number(player->getHealth()) + "\nMana: " + QString::number(player->getMana()) );
}

void RenderEngine::drawEnemyInfo(Player *player) {
    enemyInfo->setText("Życie gracza: " +
                       QString::number(player->getHealth()) +
                       "\nMana przeciwnika: " +
                       QString::number(player->getMana()) +
                       "\nKarty na ręce: " +
                       QString::number(player->getHandDeck()->getCards().size()));
}

void RenderEngine::clearPlayerHand() {
    QLayoutItem *child;
    while ((child = playerHandLayout->takeAt(0)) != 0)  {
        delete child->widget();
        delete child;
    }
}

void RenderEngine::clearPlayerTable() {
    QLayoutItem *child;
    while ((child = playerTableLayout->takeAt(0)) != 0)  {
        delete child->widget();
        delete child;
    }
}

void RenderEngine::clearEnemyTable() {
    QLayoutItem *child;
    while ((child = enemyTableLayout->takeAt(0)) != 0)  {
        delete child->widget();
        delete child;
    }
}

RenderEngine::RenderEngine(
    Game *game_,
    QHBoxLayout *playerHandLayout_,
    QHBoxLayout *playerTableLayout_,
    QHBoxLayout *enemyTableLayout_,
    QLabel *playerInfo_,
    QLabel *enemyInfo_
) {
    game = game_;
    playerHandLayout = playerHandLayout_;
    playerTableLayout = playerTableLayout_;
    enemyTableLayout = enemyTableLayout_;
    playerInfo = playerInfo_;
    enemyInfo = enemyInfo_;
}

void RenderEngine::drawActivePlayerTable() {
    clearEnemyTable();
    clearPlayerTable();
    clearPlayerHand();

    drawEnemyInfo(game->getNonActivePlayer());
    drawPlayerInfo(game->getActivePlayer());
    drawEnemyTable(game->getNonActivePlayer());
    drawPlayerTable(game->getActivePlayer());
    drawPlayerHand(game->getActivePlayer());
}

void RenderEngine::drawLocalPlayerTable() {
    clearEnemyTable();
    clearPlayerTable();
    clearPlayerHand();

    drawEnemyInfo(game->getOtherPlayer(game->getLocalPlayer()));
    drawPlayerInfo(game->getLocalPlayer());
    drawEnemyTable(game->getOtherPlayer(game->getLocalPlayer()));
    drawPlayerTable(game->getLocalPlayer());
    drawPlayerHand(game->getLocalPlayer());
}


