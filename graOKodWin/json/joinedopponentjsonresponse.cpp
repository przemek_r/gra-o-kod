#include "joinedopponentjsonresponse.h"

JoinedOpponentJsonResponse::JoinedOpponentJsonResponse(int opponentId_, std::string opponentName_, Deck *opponentDeck_) {
    opponentId = opponentId_;
    opponentName = opponentName_;
    opponentDeck = opponentDeck_;
}

int JoinedOpponentJsonResponse::getOpponentId() {
    return opponentId;
}

std::string JoinedOpponentJsonResponse::getOpponentName() {
    return opponentName;
}

Deck *JoinedOpponentJsonResponse::getOpponentDeck() {
    return opponentDeck;
}


