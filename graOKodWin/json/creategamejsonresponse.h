#ifndef CREATEGAMEJSONRESPONSE_H
#define CREATEGAMEJSONRESPONSE_H

#include <string>

/**
 * @brief Model danych odpowiedzi na stworzenie gry.
 *   
 */
class CreateGameJsonResponse {
private:
    int playerId;
    int gameId;
public:
    /**
     * @brief Konstruktor CreateGameJsonResponse
     * @param playerId
     * @param gameId
     */
    CreateGameJsonResponse(int playerId, int gameId);
    ~CreateGameJsonResponse() {}

    int getPlayerId();
    int getGameId();
};

#endif // CREATEGAMEJSONRESPONSE_H
