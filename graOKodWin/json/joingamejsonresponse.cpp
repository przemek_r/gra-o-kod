#include "joingamejsonresponse.h"

JoinGameJsonResponse::JoinGameJsonResponse(int gameId_,
                                           int playerId_,
                                           int opponentId_,
                                           std::string opponentName_,
                                           Deck *opponentDeck_) {
    gameId = gameId_;
    playerId = playerId_;
    opponentId = opponentId_;
    opponentName = opponentName_;
    opponentDeck = opponentDeck_;
}

int JoinGameJsonResponse::getGameId() {
    return gameId;
}

int JoinGameJsonResponse::getPlayerId() {
    return playerId;
}

int JoinGameJsonResponse::getOpponentId() {
    return opponentId;
}

std::string JoinGameJsonResponse::getOpponentName() {
    return opponentName;
}

Deck *JoinGameJsonResponse::getOpponentDeck() {
    return opponentDeck;
}

