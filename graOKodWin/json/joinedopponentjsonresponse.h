#ifndef JOINEDOPPONENTDATARESPONSE_H
#define JOINEDOPPONENTDATARESPONSE_H
#include <string>


class Deck;

/**
 * @brief Model danych odpowiedzi na połączenie przeciwnika.
 *  
 */
class JoinedOpponentJsonResponse {
private:
    int opponentId;
    std::string opponentName;
    Deck* opponentDeck;
public:
    /**
     * @brief Kontruktor JoinedOpponentJsonResponse
     * @param opponentId_
     * @param opponentName_
     * @param opponentDeck_
     */
    JoinedOpponentJsonResponse(int opponentId_, std::string opponentName_, Deck* opponentDeck_);

    int getOpponentId();
    std::string getOpponentName();
    Deck* getOpponentDeck();

};

#endif // JOINEDOPPONENTDATARESPONSE_H
