#ifndef JOINGAMEJSONRESPONSE_H
#define JOINGAMEJSONRESPONSE_H
#include <string>

class Deck;

/**
 * @brief Model danych odpowiedzi na dołączenie do gry
 *  
 */
class JoinGameJsonResponse {
private:
    int gameId;
    int playerId;
    int opponentId;
    std::string opponentName;
    Deck* opponentDeck;

public:
    /**
     * @brief Kontruktor JoinGameJsonResponse
     * @param gameId_
     * @param playerId_
     * @param opponentId_
     * @param opponentName_
     * @param opponentDeck_
     */
    JoinGameJsonResponse(int gameId_,
                         int playerId_,
                         int opponentId_,
                         std::string opponentName_,
                         Deck* opponentDeck_);
    ~JoinGameJsonResponse() {}

    int getGameId();
    int getPlayerId();
    int getOpponentId();
    std::string getOpponentName();
    Deck* getOpponentDeck();
};

#endif // JOINGAMEJSONRESPONSE_H
