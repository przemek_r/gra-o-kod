#ifndef OPPONENTACTIONJSONRESPONSE_H
#define OPPONENTACTIONJSONRESPONSE_H
#include "../dataparser.h"


/**
 * @brief Model danych odpowiedzi na zapytanie o akcje
 */
class OpponentActionJsonResponse {
private:
    ActionType actionType;
    int cardId;
    int targetId;
public:
    /**
     * @brief Konstruktor OpponentActionJsonResponse
     * @param actionType_
     * @param cardId_
     * @param tagetId_
     */
    OpponentActionJsonResponse(ActionType actionType_, int cardId_, int tagetId_);

    ActionType getActionType();
    int getCardId();
    int getTargetId();
};

#endif // OPPONENTACTIONJSONRESPONSE_H
