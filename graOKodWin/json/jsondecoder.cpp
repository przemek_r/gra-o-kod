#include "jsondecoder.h"
#include <string>
#include <sstream>
#include <vector>
#include "json/creategamejsonresponse.h"
#include "../rapidjson/document.h"
#include "utils/stringchararrayconverter.h"
#include "../../graOKod/stackdeckcreator.h"
#include "../../graOKod/cardmanager.h"

using namespace rapidjson;


CreateGameJsonResponse JsonDecoder::decodeCreateGameJsonResponse(std::string json) {
    Document document;
    document.Parse(StringCharArrayConverter::stringToConstCharArray(json));

    CreateGameJsonResponse response(document["playerId"].GetInt() ,document["gameId"].GetInt());

    return response;
}

JoinGameJsonResponse JsonDecoder::decodeJoinGameJsonResponse(std::string json) {
    Document document;
    document.Parse(StringCharArrayConverter::stringToCharArray(json));

    int gameId = document["gameId"].GetInt();
    int playerId = document["playerId"].GetInt();
    int opponentId = document["opponent"]["id"].GetInt();
    std::string opponentName = document["opponent"]["name"].GetString();
    Deck* opponentDeck = StackDeckCreator::createFromJson( document["opponent"]["initialDeck"]);

    return JoinGameJsonResponse(gameId, playerId, opponentId, opponentName, opponentDeck);
}

JoinedOpponentJsonResponse JsonDecoder::decodeJoinedOpponentJsonResponse(std::string json) {
    Document document;
    document.Parse(StringCharArrayConverter::stringToCharArray(json));

    int opponentId = document["id"].GetInt();
    std::string opponentName = document["name"].GetString();
    Deck* opponentDeck = StackDeckCreator::createFromJson( document["initialDeck"]);

    return JoinedOpponentJsonResponse(opponentId, opponentName, opponentDeck);
}

OpponentActionJsonResponse JsonDecoder::decodeOpponentActionJsonResponse(std::string json) {
    json = json.substr(1, json.size()-2);
    Document document;
    document.Parse(StringCharArrayConverter::stringToCharArray(json));

    ActionType actionType = ActionType(document["typeId"].GetInt());
    int cardId = document["cardId"].GetInt();
    int targetId = document["targetCardId"].GetInt();

    return OpponentActionJsonResponse(actionType, cardId, targetId);
}
