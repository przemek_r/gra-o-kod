#include "opponentactionjsonresponse.h"


OpponentActionJsonResponse::OpponentActionJsonResponse(ActionType actionType_, int cardId_, int tagetId_) {
    actionType = actionType_;
    cardId = cardId_;
    targetId = tagetId_;
}

ActionType OpponentActionJsonResponse::getActionType() {
    return actionType;
}

int OpponentActionJsonResponse::getCardId() {
    return cardId;
}

int OpponentActionJsonResponse::getTargetId() {
    return targetId;
}
