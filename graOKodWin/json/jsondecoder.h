#ifndef JSONDECODER_H
#define JSONDECODER_H

#include "json/creategamejsonresponse.h"
#include "json/joingamejsonresponse.h"
#include "json/joinedopponentjsonresponse.h"
#include "json/opponentactionjsonresponse.h"
#include <vector>
#include <string>


/**
 * @brief Dekoder JSON. wykorzystuje biblioteke RAPIDJSON.
 *  
 *   
 */
class JsonDecoder {
private:
    JsonDecoder() {}
public:
    /**
     * @brief Dekoder odpowiedzi na stworzenie gry.
     * @param json
     * @return
     */
    static CreateGameJsonResponse decodeCreateGameJsonResponse(std::string json);

    /**
     * @brief Dekoder odpowiedzi na dołączenie do gry.
     * @param json
     * @return
     */
    static JoinGameJsonResponse decodeJoinGameJsonResponse(std::string json);

    /**
     * @brief Dekoder odpowiedzi na dołączenie do gry.
     * @param json
     * @return
     */
    static JoinedOpponentJsonResponse decodeJoinedOpponentJsonResponse(std::string json);

    /**
     * @brief Dekoder odpowiedzi na zapytanie o akcje.
     * @param json
     * @return
     */
    static OpponentActionJsonResponse decodeOpponentActionJsonResponse(std::string json);
private:
    // @todo potrzebne?
    static std::vector<std::string> split(const std::string &s, char delim);
};

#endif // JSONDECODER_H
