#include "creategamejsonresponse.h"

int CreateGameJsonResponse::getPlayerId() {
    return playerId;
}

int CreateGameJsonResponse::getGameId() {
    return gameId;
}

CreateGameJsonResponse::CreateGameJsonResponse(int _playerId, int _gameId) {
    gameId = _gameId;
    playerId = _playerId;
}
