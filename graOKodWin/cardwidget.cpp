#include "cardwidget.h"
#include "QPainter"
#include "QMessageBox"
#include "../graOKod/card.h"
#include "selectedcardmanager.h"

CardWidget::CardWidget(QWidget *parent) : QWidget(parent) {
    highlighted = false;
}

void CardWidget::setCard(Card *card_) {
    card = card_;
}

void CardWidget::setDeck(DeckEnum deck_) {
    deck = deck_;
}

void CardWidget::setHighlight(bool highlight) {
    highlighted = highlight;
    update();
}

Card *CardWidget::getCard() {
    return card;
}

void CardWidget::paintEvent(QPaintEvent *event) {
    //ustawienia rysowania
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    QPen pen = QPen(Qt::black);
    pen.setWidth(6);
    painter.setPen(pen);

    QBrush brush;
    QFont font;

    //rysowanie obwodu
    if(highlighted) {
        pen.setColor(Qt::yellow);
        painter.setPen(pen);
    }
    QRect cardRect = QRect(5, 5, 150, 230);
    painter.drawRect(cardRect);

    // wypełnienie karty
    if(card->isExhausted()) {
        brush = QBrush(Qt::lightGray);
    } else {
        brush = QBrush(QColor(30,144,255));
    }
    painter.fillRect(cardRect, brush);

    // rysowanie many
    pen.setWidth(1);
    pen.setColor(Qt::black);
    painter.setPen(pen);
    QRectF manaRect = QRect(5,5, 20, 20);
    painter.drawRect(manaRect);
    painter.drawText(manaRect, Qt::AlignCenter, QString::number(card->getCost()));

    //rysowanie nazwy
    QRectF nameRect = QRectF(35,5,110,20);
    font.setPixelSize(9);
    painter.setFont(font);
    painter.drawText(nameRect, Qt::AlignCenter, QString::fromStdString(card->getName()));

    //rysowanie obrazka

    //rysowanie opisu
    QRectF descriptionRect = QRect(10,120,140,80);
    font.setPixelSize(9);
    pen.setColor(Qt::black);
    painter.setPen(pen);
    painter.setFont(font);
    painter.drawRect(descriptionRect);
    painter.drawText(descriptionRect, Qt::AlignCenter|Qt::TextWordWrap, QString::fromStdString(card->getDescription()));

    //rysowanie ataku i życia
    if (card->getAttack() >= 0) {
        QPixmap attackPix("Graphics/attack.png");
        painter.drawPixmap(10,204,30,30,attackPix);

        QPixmap bloodDropPix("Graphics/blooddrop.png");
        painter.drawPixmap(117,204,35,35,bloodDropPix);


        QRectF attackRect = QRect(10,204,30,30);
        QRectF healthRect = QRect(117,204,35,35);
        font.setPixelSize(22);
        font.setBold(true);
        painter.setFont(font);
        pen.setColor(Qt::white);
        painter.setPen(pen);
        painter.drawText(attackRect, Qt::AlignCenter, QString::number(card->getAttack()));
        painter.drawText(healthRect, Qt::AlignCenter, QString::number(card->getHealth()));
    }
}

QSize CardWidget::sizeHint() {
    return QSize(200,250);
}

QSize CardWidget::minimumSizeHint() {
    return QSize(190, 240);

}

void CardWidget::mousePressEvent(QMouseEvent * event) {
    if( deck == PlayerHandDeck ) {
        if( SelectedCardManager::getInstance().getHandCardWidget() != nullptr ) {
            SelectedCardManager::getInstance().getHandCardWidget()->setHighlight(false);
        }
        setHighlight(true);
        SelectedCardManager::getInstance().setHandCardWidget(this);
    } else if( deck == PlayerTableDeck) {
        if( SelectedCardManager::getInstance().getTableCardWidget() != nullptr) {
            SelectedCardManager::getInstance().getTableCardWidget()->setHighlight(false);
        }
        setHighlight(true);
        SelectedCardManager::getInstance().setTableCardWidget(this);
    } else if( deck == EnemyTableDeck) {
        if( SelectedCardManager::getInstance().getTargetCardWidget() != nullptr) {
            SelectedCardManager::getInstance().getTargetCardWidget()->setHighlight(false);
        }
        setHighlight(true);
        SelectedCardManager::getInstance().setTargetCardWidget(this);
    }
}

