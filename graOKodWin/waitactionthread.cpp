#include "waitactionthread.h"
#include "connection/servercontroller.h"
#include "json/jsondecoder.h"
#include <string>

void WaitActionThread::run() {
    stoped = false;
    paused = false;

    while(1) {
        {
            QMutexLocker locker(&mutex);
            if (stoped) break;
        }
        {
            QMutexLocker locker(&mutex);
            if (!paused) checkForAction();
        }
        sleep(1);
    }
}

void WaitActionThread::checkForAction() {
    std::string response = ServerController::checkForOpponentActions(game);
    if (response != "no action") {
        OpponentActionJsonResponse decodedResponse = JsonDecoder::decodeOpponentActionJsonResponse(response);

        emit gameUpdate(
            decodedResponse.getActionType(),
            decodedResponse.getCardId(),
            decodedResponse.getTargetId()
        );
    }
}

WaitActionThread::WaitActionThread() {
    paused = false;
    stoped = false;
}

void WaitActionThread::setGame(Game *game_) {
    game = game_;
}

void WaitActionThread::pause() {
    QMutexLocker locker(&mutex);
    paused = true;
}

void WaitActionThread::resume() {
    QMutexLocker locker(&mutex);
    paused = false;
}

void WaitActionThread::stop() {
    QMutexLocker locker(&mutex);
    stoped = true;
}

