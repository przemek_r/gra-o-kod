
#include "selectedcardmanager.h"
#include "cardwidget.h"

void SelectedCardManager::setHandCardWidget(CardWidget *cardWidget) {
    selectedHandCard = cardWidget;
}

void SelectedCardManager::setTableCardWidget(CardWidget *cardWidget) {
    selectedTableCard = cardWidget;
}

void SelectedCardManager::setTargetCardWidget(CardWidget *cardWidget) {
    selectedTargetCard = cardWidget;
}

void SelectedCardManager::clearSelected() {
    selectedHandCard = nullptr;
    selectedTableCard = nullptr;
    selectedTargetCard = nullptr;
}

CardWidget *SelectedCardManager::getHandCardWidget() {
    return selectedHandCard;
}

CardWidget *SelectedCardManager::getTableCardWidget() {
    return selectedTableCard;
}

CardWidget *SelectedCardManager::getTargetCardWidget() {
    return selectedTargetCard;
}

Card *SelectedCardManager::getSelectedHandCard() {
    if( selectedHandCard != nullptr ) {
        return selectedHandCard->getCard();
    } else {
        return nullptr;
    }
}

Card *SelectedCardManager::getSelectedTableCard() {
    if( selectedTableCard != nullptr ) {
        return selectedTableCard->getCard();
    } else {
        return nullptr;
    }
}

Card *SelectedCardManager::getSelectedTargetCard() {
    if( selectedTargetCard != nullptr ) {
        return selectedTargetCard->getCard();
    } else {
        return nullptr;
    }
}

SelectedCardManager::SelectedCardManager() {
    selectedHandCard = nullptr;
    selectedTableCard = nullptr;
    selectedTargetCard = nullptr;
}
