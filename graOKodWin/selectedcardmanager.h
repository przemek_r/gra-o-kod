#ifndef SELECTEDCARDMANAGER_H
#define SELECTEDCARDMANAGER_H
class CardWidget;
class Card;

/**
 * @brief Zarządza wybieraniem kart.
 *
 */
class SelectedCardManager {
public:
    static SelectedCardManager& getInstance(){
        static SelectedCardManager instance;
        return instance;
    }


    void setHandCardWidget(CardWidget* cardWidget);
    void setTableCardWidget(CardWidget* cardWidget);
    void setTargetCardWidget(CardWidget* cardWidget);

    /**
     * @brief Czyści selekcje.
     */
    void clearSelected();

    CardWidget* getHandCardWidget();
    CardWidget* getTableCardWidget();
    CardWidget* getTargetCardWidget();

    Card* getSelectedHandCard();
    Card* getSelectedTableCard();
    Card* getSelectedTargetCard();

private:
    SelectedCardManager();
    SelectedCardManager(SelectedCardManager const&);
    void operator=(SelectedCardManager const&);

    CardWidget* selectedHandCard;
    CardWidget* selectedTableCard;
    CardWidget* selectedTargetCard;

};

#endif // SELECTEDCARDMANAGER_H
