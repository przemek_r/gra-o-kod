#include "deckbuilder.h"
#include "../graOKod/cardfactory.h"
#include "../graOKod/card.h"
#include "../graOKod/cardfactory.h"
#include "QHeaderView"
#include "deckbuilderexception.h"
#include "algorithm"
#include <fstream>

void DeckBuilder::setCardListTable(QTableWidget *cardListTable_) {
    cardListTable = cardListTable_;
}

void DeckBuilder::setCountLabel(QLabel *label) {
    countLabel = label;
}

void DeckBuilder::drawCardListTable() {
    Card* card;
    cardCountVector = std::vector<int>(CardFactory::getCardNumber());
    cardCount = 0;

    // ustawienie rozmiarów listy
    cardListTable->setRowCount(CardFactory::getCardNumber());
    cardListTable->setColumnCount(6);

    // ustawienie nagłówków
    cardListTable->setHorizontalHeaderLabels(QString("Nazwa,Koszt,Atak,Życie,Opis,Ilość").split(','));

    // wypełnienie listy
    for (int i = 0; i < CardFactory::getCardNumber(); ++i) {
        QTableWidgetItem* tempItem;
        card = CardFactory::createCardBySetId(i+1,0);
        cardCountVector[i] = 0;
        //nazwa
        cardListTable->setItem(i,0, new QTableWidgetItem(QString::fromStdString(card->getName())));
        //koszt
        tempItem = new QTableWidgetItem(QString::number(card->getCost()));
        tempItem->setTextAlignment(Qt::AlignCenter);
        cardListTable->setItem(i,1, tempItem);

        if (card->getAttack() != -1) {
            //atak
            tempItem = new QTableWidgetItem(QString::number(card->getAttack()));
            tempItem->setTextAlignment(Qt::AlignCenter);
            cardListTable->setItem(i,2, new QTableWidgetItem(QString::number(card->getAttack())));
            //życie
            tempItem = new QTableWidgetItem(QString::number(card->getHealth()));
            tempItem->setTextAlignment(Qt::AlignCenter);
            cardListTable->setItem(i,3, new QTableWidgetItem(QString::number(card->getHealth())));
        }
        //opis
        cardListTable->setItem(i,4, new QTableWidgetItem(QString::fromStdString(card->getDescription())));

        //ilość
        tempItem = new QTableWidgetItem(QString::number(cardCountVector[i]));
        tempItem->setTextAlignment(Qt::AlignCenter);
        cardListTable->setItem(i,5, new QTableWidgetItem(QString::number(cardCountVector[i])));
    }

    // ustawienie opcji tablicy
    cardListTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    cardListTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    cardListTable->setSelectionMode(QAbstractItemView::SingleSelection);
    cardListTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    cardListTable->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    // ustawnie rozmiaru kolumn
    cardListTable->setColumnWidth(0,150);
    cardListTable->setColumnWidth(1,50);
    cardListTable->setColumnWidth(2,50);
    cardListTable->setColumnWidth(3,50);
    cardListTable->setColumnWidth(4,1207);
    cardListTable->setColumnWidth(5,50);

    selectedRow = -1;
}

void DeckBuilder::addCardToDeck() {
    if (selectedRow != -1 && cardCount < 30) {
        cardCountVector[selectedRow]++;
        cardCountVector[selectedRow] = (cardCountVector[selectedRow] > 2) ? 2 : cardCountVector[selectedRow];
        cardListTable->item(selectedRow,5)->setText(QString::number(cardCountVector[selectedRow]));

        cardCount = std::accumulate(cardCountVector.begin(), cardCountVector.end(), 0);
        countLabel->setText(QString::fromStdString("Talia: ") + QString::number(cardCount) + QString("/30"));
    }
}

void DeckBuilder::removeCardFromDeck() {

    if (selectedRow != -1 && cardCount > 0) {
        cardCountVector[selectedRow]--;
        cardCountVector[selectedRow] = (cardCountVector[selectedRow] < 0) ? 0 : cardCountVector[selectedRow];
        cardListTable->item(selectedRow,5)->setText(QString::number(cardCountVector[selectedRow]));

        cardCount = std::accumulate(cardCountVector.begin(), cardCountVector.end(), 0);
        countLabel->setText(QString::fromStdString("Talia: ") + QString::number(cardCount) + QString("/30"));
    }
}

void DeckBuilder::saveToFile() {
    if (cardCount != 30) {
        throw DeckBuilderException();
    } else {
        std::ofstream ofs("deck.txt");
        for (int i = 0; i < CardFactory::getCardNumber(); ++i) {
            while(cardCountVector[i]--) {
                ofs << (i+1) << std::endl;
            }
        }
        ofs.close();
    }
}

void DeckBuilder::cellSelected(int row, int column) {
    selectedRow = row;
}

