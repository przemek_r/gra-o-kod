#ifndef RENDERENGINE_H
#define RENDERENGINE_H

class Game;
class QHBoxLayout;
class Player;
class QLabel;

/**
 * @brief Klasa wyświetlająca pole gry.
 *
 */
class RenderEngine{
private:
    Game* game;
    QHBoxLayout* playerHandLayout;
    QHBoxLayout* playerTableLayout;
    QHBoxLayout* enemyTableLayout;
    QLabel* playerInfo;
    QLabel* enemyInfo;

    /**
     * @brief Rysuje rękę gracza.
     * @param player
     */
    void drawPlayerHand(Player* player);

    /**
     * @brief Rysuje stół gracz.
     * @param player
     */
    void drawPlayerTable(Player* player);

    /**
     * @brief Rysuje stół przeciwnika.
     * @param player
     */
    void drawEnemyTable(Player* player);

    /**
     * @brief Rysuje informacje o graczu.
     * @param player
     */
    void drawPlayerInfo(Player* player);

    /**
     * @brief Rysuje informacje o przeciwniku.
     * @param player
     */
    void drawEnemyInfo(Player* player);

    /**
     * @brief Czyści rękę gracza.
     */
    void clearPlayerHand();

    /**
     * @brief Czyści stół gracza.
     */
    void clearPlayerTable();

    /**
     * @brief Czyści stół przeciwnika.
     */
    void clearEnemyTable();

public:
    /**
     * @brief Konstruktor RenderEngine
     * @param game_
     * @param playerHandLayout_
     * @param playerTableLayout_
     * @param enemyTableLayout_
     * @param playerInfo_
     * @param enemyInfo_
     */
    RenderEngine(Game* game_,
                 QHBoxLayout* playerHandLayout_,
                 QHBoxLayout* playerTableLayout_,
                 QHBoxLayout* enemyTableLayout_,
                 QLabel* playerInfo_,
                 QLabel* enemyInfo_);


    /**
     * @brief Rysuje widok aktywnego gracza.
     */
    void drawActivePlayerTable();

    /**
     * @brief Rysuje widok lokalnego gracza.
     */
    void drawLocalPlayerTable();
};

#endif // RENDERENGINE_H
