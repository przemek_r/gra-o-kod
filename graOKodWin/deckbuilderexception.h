#ifndef DECKBUILDEREXCEPTION_H
#define DECKBUILDEREXCEPTION_H
#include <exception>

/**
 * @brief Wyjątek sygnalizujący błąd kreatora talii
 */
class DeckBuilderException : public std::exception {
public:
    /**
     * @brief Konstuktor DeckBuilderException
     */
    DeckBuilderException();
    virtual const char* what() const throw();
};

#endif // DECKBUILDEREXCEPTION_H
