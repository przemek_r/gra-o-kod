#include "postrequestexception.h"

PostRequestException::PostRequestException() {}
PostRequestException::~PostRequestException() {}

const char* PostRequestException::what() {
    return "Error during sending POST request";
}
