#ifndef POSTREQUESTEXCEPTION_H
#define POSTREQUESTEXCEPTION_H

#include <exception>

/**
 * @brief Wyjątek błędu zapytania POST
 *   
 */
class PostRequestException : std::exception {
public:
    PostRequestException();
    ~PostRequestException();
    const char* what();
};

#endif // POSTREQUESTEXCEPTION_H
