#ifndef CARDWIDGET_H
#define CARDWIDGET_H
#include <QWidget>

class Card;
enum DeckEnum{
    PlayerHandDeck = 0,
    PlayerTableDeck = 1,
    EnemyTableDeck = 2,
};

/**
 * @brief Klasa wyświetlająca pojedynczą kartę.
 *
 */
class CardWidget : public QWidget {
    Q_OBJECT
private:
    Card* card;
    DeckEnum deck;
    bool highlighted;
public:
    /**
     * @brief Konstruktor CardWidget
     * @param parent
     */
    explicit CardWidget(QWidget *parent = 0);
    ~CardWidget() {}

    void setCard(Card* card_);
    void setDeck(DeckEnum deck_);
    void setHighlight(bool highlight);
    Card* getCard();

protected:
    void paintEvent(QPaintEvent* event);
    QSize sizeHint();
    QSize minimumSizeHint();
    void mousePressEvent(QMouseEvent * event);
};

#endif // CARDWIDGET_H
