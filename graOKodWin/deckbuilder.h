#ifndef DECKBUILDER_H
#define DECKBUILDER_H
#include <QTableWidget>
#include <QObject>
#include <QLabel>
#include <vector>

/**
 * @brief Kreator talii. Pozwala na stworzenie własnej talii oraz zapisanie jej do pliku.
 *
 */
class DeckBuilder : public QObject{
    Q_OBJECT;
private:
    QTableWidget* cardListTable;
    QLabel* countLabel;
    std::vector<int> cardCountVector;
    int cardCount;
    int selectedRow;

public:
    explicit DeckBuilder(QObject* parent = 0) : QObject(parent) {}
    ~DeckBuilder() {}

    void setCardListTable(QTableWidget* cardListTable_);
    void setCountLabel(QLabel* label);

    /**
     * @brief Rysuje listę dostępnych kart.
     */
    void drawCardListTable();

    /**
     * @brief Dodaje kartę do talii.
     */
    void addCardToDeck();

    /**
     * @brief Usuwa kartę z talii.
     */
    void removeCardFromDeck();

    /**
     * @brief Zapisuje talię do pliku.
     */
    void saveToFile();

public slots:
    /**
     * @brief Metoda wywoływana w chwili zaznaczenia karty w liście.
     * @param row
     * @param column
     */
    void cellSelected(int row, int column);
};

#endif // DECKBUILDER_H
